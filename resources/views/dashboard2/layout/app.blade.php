<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>روح الامینی</title>
    <link rel="icon" type="image/x-icon" href="{{asset("style/assets/img/favicon.ico")}}"/>
    <link href="{{asset("style/assets/css/loader.css")}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset("style/assets/js/loader.js")}}"></script>

    <!-- BEGIN GLOBAL MANDATORY STYaLES -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
    <link href="{{asset("style/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("style/assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("style/assets/css/structure.css")}}" rel="stylesheet" type="text/css" class="structure"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
{{--    <link href="{{asset("style/plugins/apex/apexcharts.css")}}" rel="stylesheet" type="text/css">--}}
{{--    <link href="{{asset("style/assets/css/dashboard/dash_1.css")}}" rel="stylesheet" type="text/css"--}}
{{--          class="dashboard-analytics"/>--}}
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset("style/assets/css/elements/alert.css")}}">

    <link rel="stylesheet" type="text/css" href="{{asset("style/plugins/table/datatable/datatables.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("style/assets/css/forms/theme-checkbox-radio.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("style/plugins/table/datatable/dt-global_style.css")}}">


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js" integrity="sha512-WNLxfP/8cVYL9sj8Jnp6et0BkubLP31jhTG9vhL/F5uEZmg5wEzKoXp1kJslzPQWwPT1eyMiSxlKCgzHLOTOTQ==" crossorigin="anonymous"></script>

    {{--alert--}}
    <link rel="stylesheet" type="text/css" href="{{asset("style/assets/css/elements/alert.css")}}">
    <style>
        .btn-light {
            border-color: transparent;
        }
    </style>
    @yield('start')

    {{--scrum--}}
    <link href="{{asset("style/assets/css/elements/miscellaneous.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("style/assets/css/elements/breadcrumb.css")}}" rel="stylesheet" type="text/css"/>



</head>
<body class="dashboard-analytics">

<!-- BEGIN LOADER -->
<div id="load_screen">
    <div class="loader">
        <div class="loader-content">
            <div class="spinner-grow align-self-center"></div>
        </div>
    </div>
</div>
<!--  END LOADER -->
@include('dashboard2.layout.navbar')

<div class="main-container" id="container">

    <div class="overlay"></div>
    {{--<div class="search-overlay"></div>--}}
    @include('dashboard2.layout.sidebar')

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            @include('dashboard2.layout.message')

            @yield('main')

        </div>
        @include('dashboard2.layout.footer')

    </div>


</div>
<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->


{{--<script src="{{asset("style/assets/js/libs/jquery-3.1.1.min.js")}}"></script>--}}

<script src="{{asset("style/bootstrap/js/popper.min.js")}}"></script>
<script src="{{asset("style/bootstrap/js/bootstrap.min.js")}}"></script>
<script src="{{asset("style/plugins/perfect-scrollbar/perfect-scrollbar.min.js")}}"></script>
<script src="{{asset("style/assets/js/app.js")}}"></script>
<script>
    $(document).ready(function () {
        App.init();
    });
</script>
<script src="{{asset("style/assets/js/custom.js")}}"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
{{--<script src="{{asset("style/plugins/apex/apexcharts.min.js")}}"></script>--}}
{{--<script src="{{asset("style/assets/js/dashboard/dash_1.js")}}"></script>--}}
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

{{--alert--}}
{{--<script src="{{asset("style/assets/js/scrollspyNav.js")}}></script>--}}

{{--icons--}}
<script src="{{("/style/plugins/font-icons/feather/feather.min.js")}}"></script>
<script type="text/javascript">
    feather.replace();
</script>

@yield('end')


</body>
</html>
