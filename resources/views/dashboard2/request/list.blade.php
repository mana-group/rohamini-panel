@extends('dashboard2.layout.app')

@section('start')

    <link rel="stylesheet" type="text/css" href="{{("/style/plugins/dropify/dropify.min.css")}}">
    <link href="{{("/style/assets/css/users/account-setting.css")}}" rel="stylesheet" type="text/css"/>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@endsection
@section('main')
    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">

            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-8">

                    @isset($title)<h5>{{$title}}</h5>@endif

                    <table id="zero-config" class="table table-hover" style="width:100%">
                        <?php $i = 1 ?>
                        <thead>
                        <tr>
                            <th>مشاهده</th>
                            <th>ردیف</th>
                            <th>کد</th>
                            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                <th>مشتری</th>
                                <th>پرداختیهای مشتری</th>
                            @endif
                            <th>موضوع</th>
                            <th>سطح</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($requests as $request)
                            <tr>
                                <td>
                                    <a href="/request/show/{{$request->id}}">
                                        <i data-feather="eye"
                                           @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                               @if($request->seen==1 || $request->seen==3)
                                               style="color: gray"
                                               @else
                                               style="color: blue"
                                               @endif
                                           @else
                                               @if($request->seen==2 || $request->seen==3)
                                               style="color: gray"
                                               @else
                                               style="color: blue"
                                                @endif
                                            @endif
                                        ></i>
                                    </a>

                                </td>
                                <td>{{$i++}}</td>
                                <td>
                                    {{$request->id}}
                                </td>

                                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))

                                    <td>
{{--                                        <a href="/user/{{$request['user']->id}}" style="color: blue">--}}
{{--                                            {{$request['user']->name." ". $request['user']->family}}--}}
{{--                                        </a>--}}
                                    </td>
                                    <td>
                                        {{$request['paid']}}
                                    </td>
                                @endif
                                <td>
                                    {{\App\Models\RequestType::where('id' , $request->type)->first()->name}}
                                </td>

                                <td>
                                    {{\App\Models\TypeLevel::where('id' , $request->level)->first()->name}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('end')

    <script src="{{("/style/plugins/dropify/dropify.min.js")}}"></script>
    <script src="{{("/style/plugins/blockui/jquery.blockUI.min.js")}}"></script>
    <!-- <script src="plugins/tagInput/tags-input.js"></script> -->
    <script src="{{("/style/assets/js/users/account-settings.js")}}"></script>


    <script src="{{("/style/plugins/table/datatable/datatables.js")}}"></script>
    <script>
        $('#zero-config').DataTable({
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>'
                },
                "sInfo": "صفحه _PAGE_ از _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "جستجو کنید...",
                "sLengthMenu": "نتایج :  _MENU_",
                "sEmptyTable": "هیچ موردی برای نمایش وجود ندارد!",
                "sInfoEmpty": "هیچ موردی برای نمایش موجود نمی باشد!",
                "sInfoFiltered": " - فیلتر شده از _MAX_ آیتم",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });
    </script>
@endsection
