<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Turnover;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    //
    public function dashboard()
    {
        $user = Auth::user();

//        $new=Session::get('new');
        $firstLogin = false;
        if($user->first_login == 0)
        {
            $firstLogin = true;
            $user->account+=30000;

            $user->save();
            $turn = new Turnover();
            $turn->user_id = $user->id;
            $turn->request_id = '0';
            $price = 30;
            $turn->price = $price;
            $turn->description = 'بابت اولین ورود';
            $turn->save();
            $user->first_login = 1;
            $user->save();
        }
        if ($user->hasRole('admin'))
            $requests = \App\Models\Request::get();
        else
            $requests = \App\Models\Request::where('user_id', $user->id)->get();

        $end=$requests->where('status','7')->count();
        $not=$requests->where('status','!=','7')->count();


        if (Auth::user()->hasRole('admin')){
            $requests = $requests->where('status', '!=', '7')->where('seen', '!=', '1')->where('status', '!=', '3');
        } else {
            $requests = $requests->where('status', '!=', '7')->where('seen', '!=', '2')->where('status', '!=', '3');
        }
        foreach ($requests as $request) {
            $request['user'] = User::find($request->user_id);
            $request['paid'] = Turnover::where('user_id', $request->user_id)->sum('price');
            $request->append('TypeName')->toArray();
        }


        return view('dashboard2.dashboard',compact('requests' , 'end','not','firstLogin'));
    }
}
