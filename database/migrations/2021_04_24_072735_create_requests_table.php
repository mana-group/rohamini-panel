<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->integer('status')->comment('1.request 2.peigiri admin 3 user accept va pish pardakht 4 user rej  5.paid 6.nazar');
            $table->integer('type')->comment('1.logo 2.other');
            $table->integer('num');
            $table->integer('priority')->comment('1.bala 2.vasat 3.kam');
            $table->dateTime('date');
            $table->string('price_taghribi')->nullable();
            $table->string('price_final')->nullable();
            $table->string('price_pre')->nullable();
            $table->text('description')->nullable();
            $table->string('factor')->nullable();
            $table->string('file_final')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
