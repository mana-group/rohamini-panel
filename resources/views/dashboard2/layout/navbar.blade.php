<div class="header-container fixed-top">
    <header class="header navbar navbar-expand-sm">

        <ul class="navbar-nav theme-brand flex-row  text-center">
            <li class="nav-item theme-logo">
                <a href="/">
                    <img style="padding: 10px; filter: invert(100%) sepia(0%) saturate(7494%) hue-rotate(152deg) brightness(104%) contrast(101%);" src="{{asset("style/assets/img/rouh.svg")}}" class="navbar-logo" alt="logo">
                </a>
            </li>
            {{--<li class="nav-item theme-text">--}}
                {{--<a href="index.html" class="nav-link"> روح الامینی </a>--}}
            {{--</li>--}}
            <li class="nav-item toggle-sidebar">
                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-list">
                        <line x1="8" y1="6" x2="21" y2="6"></line>
                        <line x1="8" y1="12" x2="21" y2="12"></line>
                        <line x1="8" y1="18" x2="21" y2="18"></line>
                        <line x1="3" y1="6" x2="3" y2="6"></line>
                        <line x1="3" y1="12" x2="3" y2="12"></line>
                        <line x1="3" y1="18" x2="3" y2="18"></line>
                    </svg>
                </a>
            </li>
        </ul>

        @if(Auth::user()->hasRole('user'))
        <ul class="navbar-item flex-row navbar-dropdown">
            <li class="nav-item dropdown apps-dropdown more-dropdown md-hidden">
              <span style="color: white">
                موجودی حساب شما :{{Auth::user()->account}} تومان
              </span>
            </li>
        </ul>
        @endif



        <ul class="navbar-item flex-row search-ul">


        </ul>
        <ul class="navbar-item flex-row navbar-dropdown">

{{--            <p style="color: #fff; font-size: 16px; margin: 20px;">--}}
{{--                سید میثم روح المینی - طراحی با سابقه هویت بصری برند--}}
{{--            </p>--}}

            {{--<li class="nav-item dropdown user-profile-dropdown  order-lg-0 order-1">--}}
                {{--<a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown"--}}
                   {{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"--}}
                         {{--stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"--}}
                         {{--class="feather feather-settings">--}}
                        {{--<circle cx="12" cy="12" r="3"></circle>--}}
                        {{--<path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path>--}}
                    {{--</svg>--}}
                {{--</a>--}}
                {{--<div class="dropdown-menu position-absolute animated fadeInUp" aria-labelledby="userProfileDropdown">--}}
                    {{--<div class="user-profile-section">--}}
                        {{--<div class="media mx-auto">--}}
                            {{--<a href="/user/">--}}
                            {{--<img src="assets/img/90x90.jpg" class="img-fluid mr-2" alt="avatar">--}}
                            {{--<div class="media-body">--}}
                                {{--<h5>پروفایل من</h5>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="user-profile-section">--}}
                        {{--<div class="media mx-auto">--}}
                            {{--<a href="/logout">--}}
                            {{--<div class="media-body">--}}
                                {{--<h5>خروج</h5>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</li>--}}
        </ul>
    </header>
</div>
