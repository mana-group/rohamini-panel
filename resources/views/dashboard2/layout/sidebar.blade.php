<div class="sidebar-wrapper sidebar-theme">

    <nav id="sidebar">
        <div class="profile-info">
            <figure class="user-cover-image"></figure>
            <div class="user-info">
                @if(@\Illuminate\Support\Facades\Auth::user()->image)
                    {{--<img src="/files/user{{$user->image}}"--}}
                         {{--style="height: 100px;width: 100px ; margin: auto;">--}}

                    <img src="/files/user{{\Illuminate\Support\Facades\Auth::user()->image}}" alt="avatar">
                @else
                    <img src="assets/img/90x90.jpg" alt="avatar">
                @endif
                <p class="">
                    @if(Auth::user()->hasRole('admin'))
                        مدیر
                    @else
                        مشتری عزیز
                    @endif
                </p>
                <h5 class="">{{Auth::user()->name}}  {{Auth::user()->family}}</h5>
            </div>
        </div>
        <div class="shadow-bottom"></div>
        <ul class="list-unstyled menu-categories" id="accordionExample">
            <li class="menu active">
                <a href="#dashboard" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                             stroke-linejoin="round" class="feather feather-home">
                            <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                            <polyline points="9 22 9 12 15 12 15 22"></polyline>
                        </svg>
                        <span>پروژه ها</span>
                    </div>
                    <div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                             fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                             stroke-linejoin="round" class="feather feather-chevron-right">
                            <polyline points="9 18 15 12 9 6"></polyline>
                        </svg>
                    </div>
                </a>
                <ul class="collapse submenu recent-submenu mini-recent-submenu list-unstyled show" id="dashboard"
                    data-parent="#accordionExample">
                    @if(Auth::user()->hasRole('user'))
                        <li class="active">
                            <a href="/request"> پروژه جدید </a>
                        </li>
                    @endif
                    <li>
                        <a href="/request/list?fin=0"> پروژه های در دست اقدام </a>
                    </li>
                        <li>
                        <a href="/request/list?fin=1"> پروژه های تمام شده </a>
                    </li>
                        <li>
                        <a href="/request/list">همه پروژه ها  </a>
                    </li>
                </ul>
            </li>

            @if(Auth::user()->hasRole('admin') )
                <li class="menu">
                    <a href="/users" aria-expanded="false" class="dropdown-toggle">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-book">
                                <path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path>
                                <path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path>
                            </svg>
                            <span>کاربران</span>
                        </div>
                    </a>
                </li>
                <li class="menu">
                    <a href="/surveys" aria-expanded="false" class="dropdown-toggle">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-book">
                                <path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path>
                                <path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path>
                            </svg>
                            <span>سوالات نظر سنجی</span>
                        </div>
                    </a>
                </li>

            @endif
            <li class="menu">
                <a href="/user/{{\Illuminate\Support\Facades\Auth::user()->id}}" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        {{--<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
                        {{--fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
                        {{--stroke-linejoin="round" class="feather feather-eye">--}}
                        {{--<path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>--}}
                        {{--<polyline points="9 22 9 12 15 12 15 22"></polyline>--}}
                        {{--</svg>--}}
                        <i data-feather="user" style="color: blue">
                        </i>
                        <span>پروفایل</span>
                    </div>
                </a>
            </li>
            <li class="menu">
                <a href="/logout" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        {{--<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"--}}
                        {{--fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"--}}
                        {{--stroke-linejoin="round" class="feather feather-eye">--}}
                        {{--<path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>--}}
                        {{--<polyline points="9 22 9 12 15 12 15 22"></polyline>--}}
                        {{--</svg>--}}
                        <i data-feather="log-out" style="color: red">
                        </i>
                        <span>خروج</span>
                    </div>
                </a>
            </li>


        </ul>

    </nav>

</div>
