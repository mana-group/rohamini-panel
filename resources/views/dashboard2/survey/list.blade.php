@extends('dashboard2.layout.app')

@section('start')

    <link rel="stylesheet" type="text/css" href="{{("/style/plugins/dropify/dropify.min.css")}}">
    <link href="{{("/style/assets/css/users/account-setting.css")}}" rel="stylesheet" type="text/css"/>

@endsection
@section('main')


    <div class="account-settings-container layout-top-spacing">

        <div class="account-content">
            <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                 data-offset="-100">


                <form
                        @if(isset($serv))
                        action="/survey/{{$serv->id}}"
                        @else
                        action="/surveys/create"
                        @endif
                        method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">

                        <div class="col-md-12">
                            <div class="card card-success">

                                <div class="card-header">
                                    <h5 class="card-title">
                                        متن نظر سنجی
                                    </h5>
                                </div>
                                <div class="card-body">

                                    {{--//description--}}
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            توضیحات</span>
                                            </div>
                                            <textarea
                                                    style="min-height: 50px"
                                                    name="question"
                                                    type="text"
                                                    class="form-control rtl "
                                            >@if(isset($serv)){{$serv->question}}@endif</textarea>
                                        </div>
                                        @if ($errors->any()&& $errors->first('martial'))
                                            <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                    @endif

                                    <!-- /.input group -->
                                    </div>

                                </div>
                            </div>

                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <button type="submit" class="btn btn-block btn-outline-primary" id="btn">
                                @if(isset($serv))
                                بروزرسانی
                                @else
                                    ارسال
                                    @endif
                            </button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>


    <div class="col-lg-12">
        <div class="statbox widget box box-shadow">

            <div class="widget-content widget-content-area">
                <div class="table-responsive mb-8">

                    <table id="zero-config" class="table table-responsive style-3  table-hover">
                        <?php $i = 1 ?>
                        <thead>
                        <tr>
                            <th>ردیف</th>
                            <th>متن</th>
                            <th>ویرایش</th>
                            <th>حذف</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($requests as $request)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    {{$request->question}}
                                </td>

                                <td>
                                    <a href="/survey/{{$request->id}}">
                                        <i data-feather="edit" style="color: blue"></i>
                                    </a>

                                </td>
                                <td>
                                    <a href="/survey/delete/{{$request->id}}">
                                        <i data-feather="trash-2" style="color: red"></i>
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('end')

    <script src="{{("/style/plugins/dropify/dropify.min.js")}}"></script>
    <script src="{{("/style/plugins/blockui/jquery.blockUI.min.js")}}"></script>
    <!-- <script src="plugins/tagInput/tags-input.js"></script> -->
    <script src="{{("/style/assets/js/users/account-settings.js")}}"></script>


    <script src="{{("/style/plugins/table/datatable/datatables.js")}}"></script>
    <script>
        $('#zero-config').DataTable({
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>'
                },
                "sInfo": "صفحه _PAGE_ از _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "جستجو کنید...",
                "sLengthMenu": "نتایج :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });
    </script>
@endsection
