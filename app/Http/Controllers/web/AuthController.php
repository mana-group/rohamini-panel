<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use SoapClient;
use function Sodium\compare;
use Validator;
use DB;

class AuthController extends Controller
{
    //
    public function sendSms($text1, $text2, $rec, $body)
    {
        $text = $text1 . ";" . $text2;
        ini_set("soap.wsdl_cache_enabled", "0");
        $sms = new SoapClient("http://api.payamak-panel.com/post/Send.asmx?wsdl", array("encoding" => "UTF-8"));
        $data = array(
            "username" => "09130999094",
            "password" => "5243",
            "text" => "$text",
            "to" => $rec,
            "from" => "50004001999094",
            "bodyId" => $body
        );
        $send_Result = $sms->SendByBaseNumber2($data)->SendByBaseNumber2Result;
        return $send_Result;


    }
    public function sendPass(Request $request)
    {

        $new=0;
        $mobile = Session::get('mobile');
        if (strlen($request->mobile) != 11){
            return back()->with(['mobile' => $mobile, 'error' => "شماره تلفن وارد شده اشتباه است."]);
        }
        if ($request->mobile) {
            $mobile = $request->mobile;
            $user = User::where('mobile', $mobile)->first();

            if ($user) {
                if (!$user->hasRole('admin'))
                {

                    $p=mt_rand(10000,99999);
                    $pass=bcrypt($p);
                    $user->password = $pass;
                    $this->sendSms(" " . " " . " ", $p, $user->mobile, "50199");
                    $user->save();
                }

            } else

            {
                $user=new User();
                $user->mobile=$mobile;
                $user->password = bcrypt(12345);

//                $user->password = bcrypt(mt_rand(10000,99999));
                $p=mt_rand(10000,99999);
                $pass=bcrypt($p);
                $user->password = $pass;
                $this->sendSms(" " . " " . " ", $p, $user->mobile, "50199");

                $user->save();
                $role = Role::where('name', 'user')->first();
                $user->roles()->attach($role);
$new=1;
            }
        }
        $user_id = User::where('mobile', $mobile)->first()->id;

        return view('dashboard2.auth.loginsms', compact('mobile','new' , 'user_id'));


    }

    public function login(Request $request)
    {
        if ($request->isMethod("get")) {
            return view('dashboard2.auth.login2');
        } elseif ($request->isMethod("post")) {
            $user = User::where('mobile', $request->mobile)->first();
            if ($user && Hash::check($request->password, $user->password)) {
                Auth::login($user);
                if(!$user->hasRole('admin'))
                {
                    $user->password=null;
                    $user->save();
                }
                $new=$request->new;
                return redirect('/')->with('new',$new);
            } else {
                return back()->with(['mobile' => $request->mobile, 'error' => "موبایل یا رمز عبور اشتباه است"]);
            }
        } else {
            return abort('404');
        }
    }


    public function logout()
    {
        Auth::logout();
        return redirect('http://rouholaminy.ir/');
    }

    public function reg()
    {
        return view('dashboard2.auth.register2');
    }


    public function register(Request $request)
    {

        $data = $request->all();

        $rule = [
            'name' => 'required|max:255',
            'family' => 'required|max:255',
            'mobile' => 'required|max:255|unique:users',
            'password' => 'required',
            'type' => 'required',
        ];
        $message = [
            "mobile.unique" => "موبایل تکراری است",
        ];
        $valid = Validator::make($data, $rule, $message);
        if ($valid->fails())
            return redirect()->back()->withErrors($valid)->withInput();

        $user = new User();
        $user->name = $request->name;
        $user->family = $request->family;
        $user->mobile = $request->mobile;
        $user->password = bcrypt($request->password);
        $user->save();

        $role = Role::where('name', 'user')->first();
        $user->roles()->attach($role);

        Auth::login($user);
        return redirect('/');
    }

    function checkCode(Request $request)
    {
        $data = $request->all();
        $user = User::find($request->user_id);
        if ($user && Hash::check($request->password, $user->password)) {
            Auth::login($user);
            if(!$user->hasRole('admin'))
            {
                $user->password=null;
                $user->save();
            }
            $new=$request->new;
            return response()->json(['status'=>'success','message'=>'']);
        } else {
            return response()->json(['status'=>'failed','message'=>'کد وارد شده صحیح نمی باشد']);
        }

    }
}
