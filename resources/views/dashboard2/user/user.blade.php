@extends('dashboard2.layout.app')

@section('start')

    <link rel="stylesheet" type="text/css" href="{{("/style/plugins/dropify/dropify.min.css")}}">
    <link href="{{("/style/assets/css/users/account-setting.css")}}" rel="stylesheet" type="text/css"/>


    <link href="{{url('style/plugins/animate/animate.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{url('style/plugins/sweetalerts/promise-polyfill.js')}}"></script>
    <link href="{{url('style/plugins/sweetalerts/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('style/plugins/sweetalerts/sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('style/assets/css/components/custom-sweetalert.css')}}" rel="stylesheet" type="text/css" />

@endsection
@section('main')




            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                         data-offset="-100">


                        <form action="/user/edit/{{$user->id}}"
                              method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-6">
                                    <div class="card card-success">
                                        <div class="card-header">
                                            <h3 class="card-title">
                                                اطلاعات
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            {{--//name--}}
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">نام</span>
                                                    </div>
                                                    <input
                                                            placeholder="نام " type="text"
                                                            name="name"
                                                            class="form-control rtl"
                                                            value="@if(isset($user)) {{$user->name}} @endif"
                                                            @if(!$edit) disabled @endif
                                                    >
                                                </div>
                                                @if ($errors->any()&& $errors->first('name'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('name')}}</p>
                                                @endif
                                            </div>
                                            {{--family--}}
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">نام خانوادگی</span>
                                                    </div>
                                                    <input
                                                        placeholder="نام خانوادگی" type="text"
                                                        name="family"
                                                        class="form-control rtl"
                                                        value="@if(isset($user)) {{$user->family}} @endif"
                                                        @if(!$edit) disabled @endif>
                                                </div>
                                                @if ($errors->any()&& $errors->first('family'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('family')}}</p>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">تاریخ تولد</span>
                                                    </div>
                                                    <input
                                                        placeholder="تاریخ تولد" type="text"
                                                        name="birthday"
                                                        class="form-control rtl"
                                                        value="@if(isset($user)) {{$user->birthday}} @endif"
                                                        @if(!$edit) disabled @endif>
                                                </div>
                                                @if ($errors->any()&& $errors->first('birthday'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('birthday')}}</p>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">نام شرکت</span>
                                                    </div>
                                                    <input
                                                        placeholder="نام شرکت" type="text"
                                                        name="factory"
                                                        class="form-control rtl"
                                                        value="@if(isset($user)) {{$user->factory}} @endif"
                                                        @if(!$edit) disabled @endif>
                                                </div>
                                                @if ($errors->any()&& $errors->first('factory'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('factory')}}</p>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">موجودی کیف پول</span>
                                                    </div>
                                                    <input
                                                        placeholder="موجودی کیف پول" type="text"
                                                        name="factory"
                                                        class="form-control rtl"
                                                        value="@if(isset($user)) {{$user->account}} تومان @endif"
                                                        disabled >
                                                </div>
                                                @if ($errors->any()&& $errors->first('factory'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('factory')}}</p>
                                                @endif
                                            </div>
                                            {{--//national--}}
                                            {{--<div class="form-group">--}}
                                                {{--<div class="input-group">--}}
                                                    {{--<div class="input-group-prepend">--}}
                                                        {{--<span class="input-group-text">کدملی</span>--}}
                                                    {{--</div>--}}
                                                    {{--<input maxlength="11"--}}
                                                           {{--placeholder="کد ملی را وارد کنید" type="text"--}}
                                                           {{--name="national"--}}
                                                           {{--class="form-control rtl"--}}
                                                           {{--value="@if(isset($user)) {{$user->national}} @endif"--}}
                                                           {{--@if(Laratrust::hasRole('student'))--}}
                                                           {{--readonly--}}
                                                            {{--@endif>--}}
                                                {{--</div>--}}
                                                {{--@if ($errors->any()&& $errors->first('national'))--}}
                                                    {{--<p class="mt-2 text-danger mr-1">{{$errors->first('national')}}</p>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                            {{--email--}}
                                            {{--<div class="form-group">--}}
                                                {{--<div class="input-group">--}}
                                                    {{--<div class="input-group-prepend">--}}
                                                        {{--<span class="input-group-text">EMAIL</span>--}}
                                                    {{--</div>--}}
                                                    {{--<input--}}
                                                            {{--placeholder="ایمیل" type="text"--}}
                                                            {{--name="email"--}}
                                                            {{--class="form-control rtl"--}}
                                                            {{--value="@if(isset($user)) {{$user->email}} @endif">--}}
                                                {{--</div>--}}
                                                {{--@if ($errors->any()&& $errors->first('email'))--}}
                                                    {{--<p class="mt-2 text-danger mr-1">{{$errors->first('email')}}</p>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}
                                            {{--//gender--}}
                                            {{--<div class="form-group">--}}
                                                {{--<div class="input-group">--}}
                                                    {{--<div class="input-group-prepend">--}}
                                                            {{--<span class="input-group-text">--}}
                                                                        {{--جنسیت</span>--}}
                                                    {{--</div>--}}
                                                    {{--<select name="gender" @if(!$edit) disabled @endif required--}}
                                                            {{--class="form-control rtl">--}}
                                                        {{--<option @if (old('gender') == "") {{ 'selected' }} @endif value="">--}}
                                                            {{--جنسیت را--}}
                                                            {{--مشخص کنید--}}
                                                        {{--</option>--}}
                                                        {{--@if(isset($user))--}}
                                                            {{--<option value="0" @if ($user->gender== "0") {{ 'selected' }} @endif>--}}
                                                        {{--@else--}}
                                                            {{--<option value="0" @if (old('gender') == "0") {{ 'selected' }} @endif>--}}
                                                                {{--@endif--}}
                                                                {{--زن--}}
                                                            {{--</option>--}}
                                                            {{--@if(isset($user))--}}
                                                                {{--<option value="1" @if ($user->gender == "1") {{ 'selected' }} @endif>--}}
                                                            {{--@else--}}
                                                                {{--<option value="1" @if (old('gender') == "1") {{ 'selected' }} @endif>--}}
                                                                    {{--@endif--}}
                                                                    {{--مرد--}}
                                                                {{--</option>--}}
                                                    {{--</select>--}}
                                                {{--</div>--}}
                                                {{--@if ($errors->any()&& $errors->first('martial'))--}}
                                                    {{--<p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>--}}
                                            {{--@endif--}}

                                            {{--<!-- /.input group -->--}}
                                            {{--</div>--}}

                                            {{--//shenasname--}}
                                            {{--<div class="form-group">--}}
                                                {{--<div class="input-group">--}}
                                                    {{--<div class="input-group-prepend">--}}
                                                        {{--<span class="input-group-text">شماره شناسنامه</span>--}}
                                                    {{--</div>--}}
                                                    {{--<input maxlength="11"--}}
                                                           {{--placeholder="شماره شناسنامه را وارد کنید" type="text"--}}
                                                           {{--name="shenasname"--}}
                                                           {{--class="form-control rtl"--}}
                                                           {{--value="@if(isset($user)) {{$user->shenasname}} @endif"--}}
                                                           {{--@if(!$edit) disabled @endif>--}}
                                                {{--</div>--}}
                                                {{--@if ($errors->any()&& $errors->first('shenasname'))--}}
                                                    {{--<p class="mt-2 text-danger mr-1">{{$errors->first('shenasname')}}</p>--}}
                                                {{--@endif--}}
                                            {{--</div>--}}

                                            {{--//mobile--}}
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">تلفن همراه</span>
                                                    </div>
                                                    <input maxlength="11"
                                                           placeholder="تلفن همراه را وارد کنید" type="text"
                                                           @if(!$edit) disabled @endif
                                                           name="mobile"
                                                           class="form-control rtl"
                                                           value=@if(isset($user)) {{$user->mobile}}  @else"{{ old('mobile') }}"@endif>
                                                </div>
                                                @if ($errors->any()&& $errors->first('mobile'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('mobile')}}</p>
                                                @endif
                                            </div>

                                            {{--//password--}}
                                            {{--@if($edit)--}}
                                            {{--<div class="form-group">--}}
                                                {{--<div class="input-group">--}}
                                                    {{--<div class="input-group-prepend">--}}
                                                        {{--<span class="input-group-text">تغییر رمز عبور</span>--}}
                                                    {{--</div>--}}
                                                    {{--<input--}}
                                                            {{--placeholder="رمز جدید را وارد کنید" type="text"--}}
                                                            {{--name="password"--}}
                                                            {{--class="form-control rtl"--}}
                                                    {{-->--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--@endif--}}
                                            {{--image--}}
                                            @if(@\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">عکس</span>
                                                        </div>
                                                        <div class="md-form form-line">
                                                            <input type="file" class="form-control"
                                                                   name="image">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if(isset($user)&&$user->image)
                                                <img src="/files/user{{$user->image}}"
                                                     style="height: 100px;width: 100px ; margin: auto;">
                                            @endif
                                            @if ($errors->any()&& $errors->first('image'))
                                                <p class="mt-2 text-danger mr-1">{{$errors->first('image')}}</p>
                                            @endif


                                        </div>

                                    </div>

                                </div>
                            </div>
                            {{--@if($edit)--}}
                            <div class="row">
                                <div class="col-md-6 offset-md-3">
                                    <button type="submit" class="btn btn-block btn-outline-primary" id="btn">ذخیره
                                    </button>
                                </div>
                            </div>
                            {{--@endif--}}
                        </form>


                    </div>
                </div>

            </div>



@endsection

@section('end')

    <script src="{{("/style/plugins/dropify/dropify.min.js")}}"></script>
    <script src="{{("/style/plugins/blockui/jquery.blockUI.min.js")}}"></script>
    <!-- <script src="plugins/tagInput/tags-input.js"></script> -->
    <script src="{{("/style/assets/js/users/account-settings.js")}}"></script>

@endsection
