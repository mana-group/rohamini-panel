@extends('dashboard2.layout.app')

@section('start')

    <link rel="stylesheet" type="text/css" href="{{("/style/plugins/dropify/dropify.min.css")}}">
    <link href="{{("/style/assets/css/users/account-setting.css")}}" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://unpkg.com/persian-datepicker@latest/dist/css/persian-datepicker.css">
    <script src="https://unpkg.com/persian-date@latest/dist/persian-date.js"></script>
    <script src="https://unpkg.com/persian-datepicker@latest/dist/js/persian-datepicker.js"></script>

    <link href="{{("/style/assets/css/scrollspyNav.css")}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{("/style/plugins/jquery-step/jquery.steps.css")}}">
    <style>
        #formValidate .wizard > .content {
            min-height: 25em;
        }

        #example-vertical.wizard > .content {
            min-height: 24.5em;
        }
    </style>


    <link rel="stylesheet" type="text/css" href="{{("/style/assets/css/forms/theme-checkbox-radio.css")}}">


    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


@endsection
@section('main')


    @if ($message = Session::get('phase1'))
        <script type="text/javascript">
            swal("درخواست پروژه جدید شما ارسال شد . ", "منتظر تماس ما باشید.", "success");
        </script>
    @endif
    @if ($message = Session::get('phase2'))
        <script type="text/javascript">
            swal("توضیحات ثبت شد", "منتظر بررسی توسط مشتری باشید", "success");
        </script>
    @endif
    @if ($message = Session::get('acc'))
        <script type="text/javascript">
            swal(" ثبت شد", "منتظر اتمام طراحی توسط آقای روح الامین باشید", "success");
        </script>
    @endif
    @if ($message = Session::get('den'))
        <script type="text/javascript">
            swal("ثبت شد", "منتظر تماس باشید...", "success");
        </script>
    @endif
    @if ($message = Session::get('uploadadmin'))
        <script type="text/javascript">
            swal("اپلود شد", "", "success");
        </script>
    @endif
    @if ($message = Session::get('pay'))
        <script type="text/javascript">
            swal("پرداخت شد", "لطفا قبل از دانلود فایل طراحی، در نظرسنجی شرکت کنید", "success");
        </script>
    @endif
    @if ($message = Session::get('nazar'))
        <script type="text/javascript">
            swal("نظر شما ثبت شد", "اکنون فایل نهایی را دانلود کنید", "success");
        </script>
    @endif


    <div class="account-settings-container layout-top-spacing">

        <div class="account-content">
            <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                 data-offset="-100">


                <form
                    @if(!isset($request))
                    action="/request/create"

                    @elseif($request->status==1 || $request->status==3)
                    action="/request/description/{{$request->id}}"

                    @elseif($request->status==2)
                    action="/request/accessdenied/{{$request->id}}"

                    @elseif($request->status==4)
                    action="/request/finaladmin/{{$request->id}}"

                    @endif
                    method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <div class="card card-success">
                                {{--sabt--}}
                                <div class="card-header" onclick="switchFirstPanel()"
                                     onMouseOver="this.style.cursor='pointer'">
                                    <div class="row">
                                        <h3 class="card-title col">
                                            @if(isset($request))
                                                اطلاعات ثبت شده توسط مشتری
                                            @else
                                                ثبت سفارش
                                            @endif
                                        </h3>
                                        @if(isset($request) && !Auth::user()->hasRole('admin'))
                                            <img id="firstPanelArrow"
                                                 style="width: 40px; height: 40px; padding: 10px; margin-left: 10px;"
                                                 src="{{url('style/assets/img/arrow.png')}}">
                                        @endif
                                    </div>
                                </div>
                                <div class="card-body" id="firstPanel">
                                    {{--//type--}}
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            نوع سفارش</span>
                                            </div>
                                            <select name="type" required onchange="changeLevel()"
                                                    class="form-control rtl" id="type-selection"
                                                    @if(isset($request)) disabled @endif
                                            >
                                                <option value="">
                                                    نوع سفارش را
                                                    مشخص کنید
                                                </option>
                                                @foreach(\App\Models\RequestType::all() as $req)
                                                    <option value="{{$req->id}}"
                                                            @if(isset($request)) @if($request->type==$req->id) selected @endif @endif >
                                                        {{$req->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if ($errors->any()&& $errors->first('martial'))
                                            <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                    @endif


                                    <!-- /.input group -->
                                    </div>

                                    @if(Auth::user()->hasRole('admin'))
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            مشتری</span>
                                                </div>
                                                <input class="form-control rtl"
                                                       disabled
                                                       value="{{$request->user->name}} {{$request->user->family}} با شماره {{$request->user->mobile}}"
                                                >
                                                <button type="button"
                                                        onclick="location.href='{{url('user/'.$request->user->id)}}';"
                                                        style="width: 150px;" class="btn btn-block btn-outline-primary"
                                                        id="btn">پروفایل مشتری
                                                </button>
                                            </div>

                                            <!-- /.input group -->
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            سطح سفارش</span>
                                            </div>
                                            <select name="level" required id="level-selection"
                                                    class="form-control rtl" onchange="changePrice()"
                                                    @if(isset($request)) disabled @endif
                                            >
                                                @foreach(\App\Models\TypeLevel::all() as $level)
                                                    <option value="{{$level->id}}"
                                                            @if(isset($request)) @if($request->level==$level->id) selected @endif @endif >
                                                        {{$level->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if ($errors->any()&& $errors->first('martial'))
                                            <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                    @endif


                                    <!-- /.input group -->
                                    </div>

                                    <div class="row">
                                        {{--num--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">تعداد</span>
                                                    </div>
                                                    <input maxlength="4"
                                                           placeholder="تعداد سفارش وارد کنید" type="number"
                                                           name="number"
                                                           required
                                                           onchange="changePrice()"
                                                           id="request-count"
                                                           class="form-control rtl"
                                                           @if(isset($request)) disabled
                                                           value="{{$request->num}}" @else  value="1" @endif

                                                    >
                                                </div>
                                                @if ($errors->any()&& $errors->first('number'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('number')}}</p>
                                                @endif
                                            </div>
                                            {{--//priority--}}
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            درجه اهمیت</span>
                                                    </div>
                                                    <select name="priority" required
                                                            class="form-control rtl"
                                                            @if(isset($request)) disabled @endif>
                                                        <option
                                                            @if (old('gender') == "") {{ 'selected' }} @endif value="">
                                                            درجه اهمیت سفارش را
                                                            مشخص کنید
                                                        </option>
                                                        <option value="3"
                                                                @if(isset($request)) @if($request->priority==3) selected @endif @endif >
                                                            کم
                                                        </option>
                                                        <option value="2"
                                                                @if(isset($request)) @if($request->priority==2) selected @endif @endif >
                                                            متوسط
                                                        </option>
                                                        <option value="1"
                                                                @if(isset($request)) @if($request->priority==1) selected @endif @endif >
                                                            زیاد
                                                        </option>
                                                    </select>
                                                </div>
                                                @if ($errors->any()&& $errors->first('martial'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                            @endif

                                            <!-- /.input group -->
                                            </div>
                                        </div>
                                    </div>
                                    {{--//date--}}
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            تاریخ پیشنهادی تحویل</span>
                                            </div>
                                            <input maxlength="4"
                                                   placeholder="تعداد سفارش وارد کنید" type="text"
                                                   name="date"
                                                   required
                                                   class="form-control rtl datapicker @if(!isset($request))  example1 @endif"
                                                   @if(isset($request)) disabled value="{{$t}}" @endif
                                            >

                                        </div>
                                        @if ($errors->any()&& $errors->first('martial'))
                                            <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                    @endif

                                    <!-- /.input group -->
                                    </div>
                                    {{--//price--}}
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            مبلغ تقریبی (تومان)</span>
                                            </div>
                                            <input maxlength="4"
                                                   id="price-text"
                                                   type="text"
                                                   name="price_taghribi"
                                                   disabled
                                                   class="form-control rtl "
                                                   required
                                            >

                                        </div>
                                        @if ($errors->any()&& $errors->first('martial'))
                                            <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                    @endif

                                    <!-- /.input group -->
                                    </div>
                                    {{--//charge--}}
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            شارژ کیف پول بصورت هدیه بابت این پروژه</span>
                                            </div>
                                            <input
                                                type="text"
                                                disabled
                                                value="10% هزینه پروژه"
                                                class="form-control rtl"
                                            >

                                        </div>
                                        @if ($errors->any()&& $errors->first('martial'))
                                            <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                    @endif

                                    <!-- /.input group -->
                                    </div>
                                </div>


                                @if(isset($request))
                                    {{--peigir--}}
                                    @if($request->status >= 4 ||$request->status == 2 || Auth::user()->hasRole('admin') )

                                        <div class="card-header">
                                            <h5 class="card-title">
                                                پیگیری سفارش
                                            </h5>
                                        </div>
                                        <div class="card-body">

                                            {{--//description--}}
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            توضیحات</span>
                                                    </div>
                                                    <textarea
                                                        style="min-height: 150px"
                                                        placeholder="تعداد،درجه اهمیت ، نوع سفارش و توضیحات تکمیلی را ذکر کنید..."
                                                        name="description"
                                                        type="text"
                                                        class="form-control rtl "
                                                        @if($request->status>1 && $request->status!= 3 )disabled @endif
                                                    >@if($request->status>1){{$request->description}}@endif</textarea>
                                                </div>
                                                @if ($errors->any()&& $errors->first('martial'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                            @endif

                                            <!-- /.input group -->
                                            </div>
                                            {{--//final price--}}
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            بودجه نهایی (تومان)</span>
                                                    </div>
                                                    <input
                                                        onkeyup="format(this)"
                                                        type="text"
                                                        name="final_price"
                                                        class="form-control rtl "
                                                        @if($request->status>1 && $request->status!= 3 )disabled
                                                        @endif
                                                        @if($request->status>1) value="{{$request->price_final}}" @endif
                                                    >

                                                </div>
                                                @if ($errors->any()&& $errors->first('martial'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                            @endif

                                            <!-- /.input group -->
                                            </div>

                                            @if($request->status >= 2  )
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                                                        @if($request->status == 3  )

                                                    پیش پرداخت قبلی (30%) (تومان)
                                                @else
                                                    پیش پرداخت (30%) (تومان)
                                                @endif
                                            </span>
                                                        </div>
                                                        <input
                                                            onload="format(this)"
                                                            type="text"
                                                            class="form-control rtl "
                                                            value="{{round(str_replace(",","",$request->price_final)/3,0)}}"
                                                            disabled
                                                            id="prepaid"
                                                        >

                                                    </div>
                                                    @if ($errors->any()&& $errors->first('martial'))
                                                        <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                                @endif

                                                <!-- /.input group -->
                                                </div>

                                                @if($request->status != 3  && !Auth::user()->hasRole('admin'))
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                موجودی کیف پول (تومان)
                                            </span>
                                                            </div>
                                                            <input
                                                                onload="format(this)"
                                                                type="text"
                                                                class="form-control rtl "
                                                                value="{{round(str_replace(",","",\Illuminate\Support\Facades\Auth::user()->account),0)}}"
                                                                disabled
                                                                id="prepaid"
                                                            >

                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                @endif
                                                @if($request->status != 3  && !Auth::user()->hasRole('admin'))
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                مبلغ پرداختی شما (کسر مبلغ کیف پول) (تومان)
                                            </span>
                                                            </div>

                                                            <input
                                                                onload="format(this)"
                                                                type="text"
                                                                class="form-control rtl "
                                                                @if(round(str_replace(",","",(str_replace(",","",$request->price_final)/3) - \Illuminate\Support\Facades\Auth::user()->account),0) >= 0)
                                                                value="{{round(str_replace(",","",(str_replace(",","",$request->price_final)/3) - \Illuminate\Support\Facades\Auth::user()->account),0)}}"
                                                                @else
                                                                value="رایگان"
                                                                @endif
                                                                disabled
                                                                id="prepaid"
                                                            >



                                                        </div>
                                                        <!-- /.input group -->
                                                    </div>
                                                @endif
                                            @endif

                                        </div>

                                    @endif
                                    @if($request->status >= 4 && Auth::user()->hasRole('admin') )

                                        {{--final admin--}}
                                        <div class="card-header">
                                            <h5 class="card-title">
                                                بارگزاری فایل نهایی
                                            </h5>
                                        </div>
                                        <div class="card-body">

                                            {{--//description--}}
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            بارگزاری فایل</span>
                                                    </div>
                                                    <input type="file" class="form-control" name="file">
                                                </div>
                                                @if ($errors->any()&& $errors->first('martial'))
                                                    <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                            @endif

                                                @if($request->is_finished == '0')
                                                    <button formaction="/request/accept-pay/{{$request->id}}" type="submit" class="btn btn-block btn-outline-primary" style="margin-top: 20px;">
                                                        کاربر پرداخت را انجام داد
                                                    </button>
                                                @endif

                                            <!-- /.input group -->
                                            </div>


                                        </div>

                                    @endif

                                    @if($request->status == 7 && Auth::user()->hasRole('admin'))

                                        {{--final admin--}}
                                        <div class="card-header">
                                            <h5 class="card-title">
                                                نتیجه نظر سنجی </h5>
                                        </div>
                                        <div class="card-body">

                                            {{--//description--}}
                                            @foreach($survs as $surv)
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            {{$surv->question}}</span>
                                                        </div>
                                                        <input
                                                            type="text"
                                                            class="form-control rtl "
                                                            value="{{$surv->ScoreName}}"
                                                            disabled
                                                        >

                                                    </div>
                                                    @if ($errors->any()&& $errors->first('martial'))
                                                        <p class="mt-2 text-danger mr-1">{{$errors->first('martial')}}</p>
                                                @endif

                                                <!-- /.input group -->
                                                </div>
                                            @endforeach
                                            @if($request->user_survey)
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            توضیح کاربر</span>
                                                        </div>
                                                        <textarea
                                                            disabled
                                                            style="min-height: 150px"
                                                            type="text"
                                                            class="form-control rtl "
                                                        >{{$request->user_survey}}</textarea>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                            @endif

                                        </div>

                                    @endif



                                @endif
                            </div>

                        </div>


                    </div>

                    @if(!isset($request))
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <button type="submit" class="btn btn-block btn-outline-primary" id="btn">ارسال درخواست
                                </button>
                            </div>
                        </div>
                    @else
                        @if($request->status == 2 && Auth::user()->hasRole('user'))

                            <span style="color: #568203; font-size: 18px; width: 100%; display: block; text-align: center; margin: 15px;">
                                                        لطفا این مبلغ را به شماره کارت X به نام روح المینی واریز نمایید. سپس دکمه کار را شروع کنید را کلیک کنید.
                                                    </span>
                            <div class="row">
                                <div class="col-md-5 offset-md-2    ">
                                    <button type="submit" class="btn btn-block btn-outline-success" id="btn">
                                        مبلغ بیعانه را پرداخت کرده ام. کار را شروع کنید.
                                    </button>
                                </div>
                                <div class="col-md-3 ">
                                    <input class="btn btn-block btn-outline-danger"
                                           style="cursor: pointer;"
                                           type="submit" name="action" value="با من تماس بگیرید">
                                </div>
                            </div>
                        @elseif(($request->status == 1 || $request->status == 3) && Auth::user()->hasRole('admin'))
                            <div class="row">
                                <div class="col-md-8 offset-md-2">
                                    <button type="submit" class="btn btn-block btn-outline-primary" id="btn">
                                        ثبت
                                    </button>
                                </div>
                            </div>
                        @elseif($request->status >= 4 && Auth::user()->hasRole('admin'))
                            <div class="row">
                                <div class="col-md-8 offset-md-2">
                                    <button type="submit" class="btn btn-block btn-outline-primary" id="btn">
                                        @if($request->status == 4)
                                            ثبت فایل نهایی
                                        @else
                                            بروز رسانی فایل نهایی
                                        @endif
                                    </button>
                                </div>
                            </div>
                        @endif

                    @endif

                </form>

                {{----}}
                @if(isset($request))
                    @if($request->status > 4 && Auth::user()->hasRole('user') )

                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8" id="final-section">
                                <div class="card card-success">
                                    {{--final--}}
                                    <div class="card-header">
                                        <h5 class="card-title">
                                            اتمام پروژه
                                        </h5>
                                    </div>
                                    <div class="card-body">
                                        {{--final--}}

                                        <div id="circle-basic" class="">
                                            @if($request->status ==5)
                                                <h3>پرداخت </h3>
                                                <section>
                                                    @if($request->status ==5  )
                                                        <span style="color: #282828; font-size: 15px; width: 100%; display: block; text-align: center; margin: 5px;">
                                                        مبلغ کل: {{round(((int) str_replace("," , "" , $request->price_final)))}} تومان
                                                        </span>
                                                        <span style="color: #282828; font-size: 15px; width: 100%; display: block; text-align: center; margin: 5px;">
                                                        مبلغ پیش پرداخت: {{round(((int) str_replace("," , "" , $request->price_final)) / 3)}} تومان
                                                        </span>
                                                        <span style="color: #282828; font-size: 15px; width: 100%; display: block; text-align: center; margin: 5px;">
                                                        کیف پول: {{\Illuminate\Support\Facades\Auth::user()->account}} تومان
                                                        </span>
                                                        <span style="color: #568203; font-size: 25px; width: 100%; display: block; text-align: center; margin: 15px;">
                                                        مبلغ باقی مانده: @if((round(((int) str_replace("," , "" , $request->price_final)) * 2 / 3) - \Illuminate\Support\Facades\Auth::user()->account) > 0) {{round(((int) str_replace("," , "" , $request->price_final)) * 2 / 3) - \Illuminate\Support\Facades\Auth::user()->account}} تومان @else رایگان @endif
                                                        </span>
                                                        <span style="color: #568203; font-size: 18px; width: 100%; display: block; text-align: center; margin: 15px;">
                                                        لطفا مابقی مبلغ را به شماره کارت X به نام روح المینی واریز نمایید. سپس بر روی 'پرداخت با موفقیت انجام شد' کلیک کنید.
                                                        </span>
                                                        <a href="/request/paid/{{$request->id}}" style="margin: auto; display: block;"
                                                           class="btn btn-primary">
                                                            پرداخت با موفقیت انجام شد
                                                        </a>
                                                    @elseif($request->status >5  )
                                                        <p>پرداخت انجام شده است</p>
                                                    @endif
                                                </section>
                                            @endif
                                            @if($request->status ==6 && $request->is_finished == 0)
                                                <h3>انتظار تایید </h3>
                                                <section>
                                                        <span style="color: #568203; font-size: 15px; width: 100%; display: block; text-align: center; margin: 5px;">
                                                    مبلغ باقی مانده: {{round(((int) str_replace("," , "" , $request->price_final)) * 2 / 3)}} تومان
                                                    </span>
                                                        <span style="color: #568203; font-size: 18px; width: 100%; display: block; text-align: center; margin: 15px;">
                                                    لطفا منتظر بمانید تا مدیریت پرداخت شما را تایید کنید.
                                                    </span>
                                                </section>
                                            @endif
                                            @if($request->status == 5 || $request->status == 6)
                                                <h3>
                                                    نظرسنجی
                                                </h3>
                                                <section>
                                                    @if($request->status ==5  )
                                                        <p>ابتدا پرداخت را انجام دهید</p>

                                                    @elseif($request->status ==6  )

                                                        <form
                                                            action="/request/nazar/{{$request->id}}"
                                                            method="post" enctype="multipart/form-data">
                                                            @csrf

                                                            @foreach($surveys as $key=>$survey)
                                                                <p class="mb-4">
                                                                    {{$survey->question}}
                                                                </p>

                                                                <div class="row">
                                                                    <div class="col col-md-2">
                                                                        <div class="n-chk">
                                                                            <label
                                                                                class="new-control new-radio new-radio-text radio-warning">
                                                                                <input type="radio"
                                                                                       checked
                                                                                       class="new-control-input form-control rtl"
                                                                                       name="answer[{{$survey->id}}]"
                                                                                       value="1"
                                                                                >
                                                                                <span
                                                                                    class="new-control-indicator"></span>عالی
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-2">
                                                                        <div class="n-chk">
                                                                            <label
                                                                                class="new-control new-radio new-radio-text radio-warning">
                                                                                <input type="radio"
                                                                                       class="new-control-input form-control rtl"
                                                                                       value="2"
                                                                                       name="answer[{{$survey->id}}]">
                                                                                <span
                                                                                    class="new-control-indicator"></span>خوب
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-3">
                                                                        <div class="n-chk">
                                                                            <label
                                                                                class="new-control new-radio new-radio-text radio-warning">
                                                                                <input type="radio"
                                                                                       class="new-control-input form-control rtl"
                                                                                       value="3"
                                                                                       name="answer[{{$survey->id}}]">
                                                                                <span
                                                                                    class="new-control-indicator"></span>متوسط
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-2">
                                                                        <div class="n-chk">
                                                                            <label
                                                                                class="new-control new-radio new-radio-text radio-warning">
                                                                                <input type="radio"
                                                                                       class="new-control-input form-control rtl"
                                                                                       value="4"
                                                                                       name="answer[{{$survey->id}}]">
                                                                                <span
                                                                                    class="new-control-indicator"></span>بد
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col col-md-3">
                                                                        <div class="n-chk">
                                                                            <label
                                                                                class="new-control new-radio new-radio-text radio-warning">
                                                                                <input type="radio"
                                                                                       class="new-control-input form-control rtl"
                                                                                       value="5"
                                                                                       name="answer[{{$survey->id}}]">
                                                                                <span
                                                                                    class="new-control-indicator"></span>خیلی
                                                                                بد
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            {{--comment--}}
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            توضیح</span>
                                                                    </div>
                                                                    <textarea
                                                                        style="min-height: 150px"
                                                                        name="user_survey"
                                                                        type="text"
                                                                        class="form-control rtl "
                                                                    ></textarea>
                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>

                                                            <button type="submit" class="btn btn-primary">
                                                                ثبت نظر
                                                            </button>
                                                        </form>


                                                    @elseif($request->status ==7  )
                                                        <p>نظر سنجی انجام شده است</p>
                                                    @endif
                                                </section>
                                            @endif
                                            <h3 id="tab-3"> دانلود فاکتور و فایل نهایی </h3>
                                            <section>
                                                @if($request->status ==5  )
                                                    <p>ابتدا پرداخت را انجام دهید</p>

                                                @elseif($request->status ==6  )
                                                    <p>نظر سنجی را انجام دهید</p>

                                                @elseif($request->status ==7  )
                                                    {{--<a class="btn btn-primary">--}}
                                                    {{--دانلود فاکتور--}}
                                                    {{--</a>--}}
                                                    <a href="{{asset('files/final/'.$request->file_final)}}"
                                                       class="btn btn-primary" download>
                                                        دانلود فایل نهایی
                                                    </a>
                                                @endif
                                            </section>
                                        </div>


                                    </div>


                                </div>
                            </div>
                        </div>
                    @endif
                @endif


                {{--@if($edit)--}}


            </div>
        </div>

    </div>



@endsection

@section('end')




    <script src="{{("/style/plugins/dropify/dropify.min.js")}}"></script>
    <script src="{{("/style/plugins/blockui/jquery.blockUI.min.js")}}"></script>
    <!-- <script src="plugins/tagInput/tags-input.js"></script> -->
    <script src="{{("/style/assets/js/users/account-settings.js")}}"></script>

    <script src="{{("/style/plugins/jquery-step/jquery.steps.min.js")}}"></script>
    <script src="{{("/style/plugins/jquery-step/custom-jquery.steps.js")}}"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $(".example1").persianDatepicker({
                    format: 'YYYY/MM/DD',
                    responsive: true,
                    timePicker: {
                        enabled: false,
                        meridiem: {
                            enabled: true
                        }
                    },
                    toolbox: {
                        submitButton: {
                            enabled: true
                        }
                    }
                }
            );
        });
    </script>
    <script>
        function format(input) {
            var nStr = input.value + '';
            nStr = nStr.replace(/\,/g, "");
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            input.value = x1 + x2;
        }

        function format2(input) {
            var nStr = input;
            nStr = nStr.replace(/\,/g, "");
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        document.getElementById("prepaid").value = format2(document.getElementById("prepaid").value);

    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="https://unpkg.com/persian-date@1.1.0/dist/persian-date.min.js"></script>
    <script src="https://unpkg.com/persian-datepicker@1.2.0/dist/js/persian-datepicker.min.js"></script>
    <script>


        @if(isset($request) && !Auth::user()->hasRole('admin'))

        document.getElementById('firstPanel').style.display = "none";

        function switchFirstPanel() {
            if (document.getElementById('firstPanel').style.display == "none") {
                document.getElementById('firstPanel').style.display = "block"

                document.getElementById('firstPanelArrow').style.webkitTransform = 'rotate(' + 90 + 'deg)';
                document.getElementById('firstPanelArrow').style.mozTransform = 'rotate(' + 90 + 'deg)';
                document.getElementById('firstPanelArrow').style.msTransform = 'rotate(' + 90 + 'deg)';
                document.getElementById('firstPanelArrow').style.oTransform = 'rotate(' + 90 + 'deg)';
                document.getElementById('firstPanelArrow').style.transform = 'rotate(' + 90 + 'deg)';
            } else {
                document.getElementById('firstPanel').style.display = "none"
                document.getElementById('firstPanelArrow').style.webkitTransform = 'rotate(' + 0 + 'deg)';
                document.getElementById('firstPanelArrow').style.mozTransform = 'rotate(' + 0 + 'deg)';
                document.getElementById('firstPanelArrow').style.msTransform = 'rotate(' + 0 + 'deg)';
                document.getElementById('firstPanelArrow').style.oTransform = 'rotate(' + 0 + 'deg)';
                document.getElementById('firstPanelArrow').style.transform = 'rotate(' + 0 + 'deg)';
            }
        }

        @endif

        @if(isset($request))
        document.getElementById('price-text').value = format2(({{\App\Models\TypeLevel::where('id' , $request->level)->first()->price}} * {{$request->num}}).toString()) + " تومان";
        @endif

        @if(isset($request) && ($request->status == 6 || $request->status == 7))
        document.getElementById("final-section").scrollIntoView();
        @endif

        $(document).ready(function () {
            $(".datapicker").pDatepicker({
                format: 'YYYY/MM/DD',
            });
            $("#form").submit(function () {
                $("#factories").prop('disabled', true);
                return true;
            });

            @if(isset($request) && $request->status ==7)
            document.getElementById("tab-3").click();
            @endif
        });

        selectedType = 1;

        var dicomarray = @JSON(\App\Models\TypeLevel::all());
        console.log(dicomarray);

        function changeLevel() {
            selectedType = parseInt(document.getElementById('type-selection').value);
            fillLevels();
        }

        function changePrice() {
            var selectedLevel = parseInt(document.getElementById('level-selection').value);
            dicomarray.forEach(addItems);

            function addItems(item) {
                if (item.id == selectedLevel) {

                    document.getElementById('price-text').value = format2((item.price * parseInt(document.getElementById('request-count').value)).toString()) + ' تومان';

                }
            }
        }

        function fillLevels() {
            var select = document.getElementById('level-selection');

            while (select.options.length > 0) {
                select.remove(select.options.length - 1);
            }

            var opt = document.createElement('option');

            opt.text = 'انتخاب کنید';
            opt.value = "";

            select.add(opt, null);

            dicomarray.forEach(addItems);

            function addItems(item) {
                if (item.type_id == selectedType) {

                    var opt = document.createElement('option');

                    opt.text = item.name;
                    opt.value = item.id;

                    select.add(opt, null);

                }
            }
        }

    </script>
@endsection
