<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Turnover;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class UserController extends Controller
{
    //
    public function list()
    {

        $users=User::whereRoleIs('user')->get();
        foreach ($users as $user)
        {
            $request=\App\Models\Request::where('user_id',$user->id)->orderBy('id','asc')->first();
            if($request)
                $user['first']=$request->created_at;
            else
                $user['first']="ندارد";
        }
        return view('dashboard2.user.list',compact('users'));
    }

    public function profile(Request $request, $id)
    {
        $user = User::findOrFail($id);
        if ($user->hasRole('admin'))
            $role = 'admin';
        else
            $role = 'user';
        $edit = '0';
        if (Auth::user()->id == $user->id || (Auth::user()->hasRole('admin')))
            $edit = '1';
        else
            abort(401);
        return view('dashboard2.user.user', compact('user', 'edit', 'role'));
    }

    public function edit(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'mobile' => 'required|max:255|unique:users,mobile,' . $id,
            'name' => 'required',
            'factory' => 'required',
            'birthday' => 'required',
            'family' => 'required'
        ]);
        if ($valid->fails()) {
            return back()->withErrors($valid);
        }
        $user = User::findOrFail($id);

        if (!(Auth::user()->id == $user->id || (Auth::user()->hasRole('admin')) ))
            abort(401);

        $user->family = $request->family;
        $user->name = $request->name;
        $user->factory = $request->factory;
        $user->birthday = $request->birthday;
        $user->mobile = $request->mobile;
        if (isset($request->password))
            $user->password = bcrypt($request->password);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = $id  . "_" . time() . "." . $image->getClientOriginalExtension();
            $destination_path = 'files/user';
            $image->move($destination_path, $image_name);
            $user->image = '/' . $image_name;
        }

        $message = 'کاربر با موفقیت ویرایش شد';
        if ($user->profile_complete == 0){
            $user->account+=30000;
            $user->profile_complete = 1;
            $user->save();

            $turn = new Turnover();
            $turn->user_id = $user->id;
            $turn->request_id = '0';
            $price = 30;
            $turn->price = $price;
            $turn->description = 'بابت کامل کردن پروفایل';
            $turn->save();
            $message = 'اطلاعات کاربر با موفقیت ویرایش شد. همچنین به علت پرکردن پروفایل به عنوان هدیه مبلغ 30 هزار تومان به حساب کاربری اضافه شد.';
        }
        $user->save();



        return redirect('/')->with('success', $message);
    }

    public function remove($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return back()->with('success', 'کاربر با موفقیت حذف شد');

    }
}
