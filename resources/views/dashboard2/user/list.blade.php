@extends('dashboard2.layout.app')

@section('start')

    <link rel="stylesheet" type="text/css" href="{{("/style/plugins/dropify/dropify.min.css")}}">
    <link href="{{("/style/assets/css/users/account-setting.css")}}" rel="stylesheet" type="text/css"/>

@endsection
@section('main')

            <div class="col-lg-12">
                <div class="statbox widget box box-shadow">

                    <div class="widget-content widget-content-area">
                        <div class="table-responsive mb-8">

                            <table id="zero-config" class="table table-responsive style-3  table-hover">
                                <?php $i = 1 ?>
                                <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>نام</th>
                                    <th>موبایل</th>
                                    <th>تاریخ اولین سفارش</th>
                                    <th>کل مبلغ دریافتی در سایت</th>
                                    <th>کل مبلغ تخفیف</th>
                                    {{--<th>تاریخ تولد</th>--}}
                                    {{--<th>گزارش ها و نظر سنجی ها</th>--}}
                                    <th>کیف پول</th>
                                    {{--<th>پیام شخصی به بنده</th>--}}
                                    <th>حذف</th>
                                    <th>پروفایل</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td><a href="/dashboard/user/{{$user->id}}">
                                                {{$user->name}}   {{$user->family}}
                                            </a>
                                        </td>
                                        <td>{{$user->mobile}}</td>
                                        <td>{{$user->first}}</td>
                                        <td></td>
                                        <td></td>
                                        <td>{{$user->account}}</td>
                                        <td>
                                            <a href="/user/remove/{{$user->id}}"
                                               onclick="return confirm('مطمئن هستید؟  ')">
                                                <i data-feather="trash" style="color: red"></i>
                                            </a>

                                        </td>
                                        <td>
                                            <a href="/user/{{$user->id}}">
                                                <i data-feather="eye" style="color: blue"></i>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

@endsection

@section('end')

    <script src="{{("/style/plugins/dropify/dropify.min.js")}}"></script>
    <script src="{{("/style/plugins/blockui/jquery.blockUI.min.js")}}"></script>
    <!-- <script src="plugins/tagInput/tags-input.js"></script> -->
    <script src="{{("/style/assets/js/users/account-settings.js")}}"></script>


    <script src="{{("/style/plugins/table/datatable/datatables.js")}}"></script>
    <script>
        $('#zero-config').DataTable({
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>' },
                "sInfo": "صفحه _PAGE_ از _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "جستجو کنید...",
                "sLengthMenu": "نتایج :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });
    </script>
@endsection
