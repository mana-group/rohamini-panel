<link href="{{asset("style/assets/css/elements/breadcrumb.css")}}" rel="stylesheet" type="text/css"/>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>روح الامین</title>
    <link rel="icon" type="image/x-icon" href="{{asset("style/assets/img/favicon.ico")}}"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
    <link href="{{asset("style/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("style/assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("style/assets/css/structure.css")}}" rel="stylesheet" type="text/css" class="structure"/>
    <link href="{{asset("style/assets/css/authentication/form-1.css")}}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset("style/assets/css/forms/theme-checkbox-radio.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("style/assets/css/forms/switches.css")}}">

    <style>
        .round-corner-input {
            border: 2px solid #565755 !important;
            color: #578600 !important;
            border-radius: 60px !important;
            padding: 0 !important;
            text-align: center;
            margin-top: 7px;
            padding-left: 60px !important;
        }

        .round-corner-input:focus {
            border: 2px solid #578600 !important;
        }

        .cycleTimerBox {
            display: inline-block;
            width: 200px;
            height: 200px;
            border-radius: 50%;
            background-color: #fff;
            float: left;
        }

    </style>

</head>
<body class="form">
@if ($message = Session::get('success'))
    <div class="alert alert-light-warning mb-4" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                 class="feather feather-x close" data-dismiss="alert">
                <line x1="18" y1="6" x2="6" y2="18"></line>
                <line x1="6" y1="6" x2="18" y2="18"></line>
            </svg>
        </button>
        <strong>هشدار!</strong>
        {{ $message }}    </div>
@endif

{{--@if ($message = Session::get('error'))--}}
{{--<div class="alert alert-light-danger mb-4" role="alert">--}}
{{--<button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
{{--<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="alert"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>--}}
{{--</button>--}}
{{--<strong>هشدار!</strong>--}}
{{--{{ $message }}    </div>--}}
{{--@endif--}}


@if ($message = Session::get('warning'))
    <div class="alert alert-light-warning mb-4" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                 class="feather feather-x close" data-dismiss="alert">
                <line x1="18" y1="6" x2="6" y2="18"></line>
                <line x1="6" y1="6" x2="18" y2="18"></line>
            </svg>
        </button>
        <strong>هشدار!</strong>
        {{ $message }}    </div>
@endif


@if ($message = Session::get('info'))
    <div class="alert alert-light-info mb-4" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                 class="feather feather-x close" data-dismiss="alert">
                <line x1="18" y1="6" x2="6" y2="18"></line>
                <line x1="6" y1="6" x2="18" y2="18"></line>
            </svg>
        </button>
        <strong>هشدار!</strong>
        {{ $message }}
    </div>
@endif

@if($errors->any())
    <div class="alert alert-info alert-block">

        {{ implode('', $errors->all(':message')) }}
    </div>
@endif


<div class="form-container">
    <div class="form-form">
        <div class="form-form-wrap">
            <div class="form-container">
                <div class="form-content">

                    {{--                    <h1 class=""><a href="#"><span class="brand-name"--}}
                    {{--                                                   style="color: #568203;">سید میثم روح الامینی</span></a></h1>--}}
                    <h5 class="" style="text-align: center;"><span class="brand-name"
                                                                   style="color: #568203;"> سید میثم روح الامینی</span>
                        <h5 class="" style="text-align: center; margin-bottom: 150px;"><span class="brand-name"
                                                                                             style="color: #568203;"> طراح با سابقه هویت بصری برند</span>
                        </h5>
                        {{--<p class="signup-link">حساب ندارید؟ <a href="/reg"> یک حساب کاربری ایجاد کنید</a></p>--}}
                        <form class="text-left" method="post" action="login" id="send-sms-form">
                            @csrf

                            <div class="form">

                                <input hidden id="new" name="new" type="text" class="form-control" placeholder="موبایل"
                                       value={{$new}} >

                                <div id="username-field" class="field-wrapper input" hidden>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-user">
                                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="12" cy="7" r="4"></circle>
                                    </svg>
                                    <input id="username" name="mobile" type="text" class="form-control"
                                           placeholder="موبایل"
                                           value="{{$mobile}}">
                                </div>

                                <p style="margin: 10px; color: #578600;">پیامک کد ورود به {{$mobile}} ارسال شد.</p>
                                <div class="row" style="width: 350px;">
                                    <img src="{{url('/style/assets/img/rouh.svg')}}" alt="Logo"
                                         style="height: 60px; position: absolute; margin-right: 290px; background-color: #fff; border-radius: 50%;">
                                    <input id="password" name="password" type="password"
                                           class="form-control round-corner-input"
                                           placeholder="* * * *">
                                    @if ($message = Session::get('error'))
                                        <p style="margin: 10px; color: #b80000;">{{$message}}</p>
                                    @endif
                                    <p style="margin: 10px; color: #b80000;" id="error-text"></p>
                                    <div class="row">
                                    <span class="cycleTimerBox"
                                          style="height: 50px; width: 50px; margin-right: 20px; display: block;">
                                        <canvas height="50px" width="50px" id="cycleTimer"/>
                                    </span>
                                        <p style="margin: 20px 0px; color: #535353;">لطفا کد پیامک شده را وارد کنید
                                            ...</p>
                                    </div>
                                </div>

                                <div class="d-sm-flex justify-content-between">
                                    <div class="field-wrapper">
                                        <button style="margin-top: 20px;" type="button" onclick="login()"
                                                class="btn btn-primary" value="" id="check-login">
                                            ورود
                                        </button>
                                    </div>
                                </div>

                                <div class="field-wrapper">
                                    <a href="/"><p style="margin: 10px; color: #578600; text-align: left;">پیامک برام
                                            ارسال
                                            نمیشه<br/>می خوام شماره رو تغییر بدم</p></a>
                                </div>

                            </div>
                        </form>
                        <p class="terms-conditions" style="color: lightgrey">Copyright 2020-2021 rouholaminy.ir All
                            Rights
                            Reserved© </p>

                </div>
            </div>
        </div>
    </div>
    <div class="form-image">
        <div class="l-image"
             style="background-color: #578600; background-image: url('{{url('/style/assets/img/login-logo.jpg')}}'); background-size: 500px;">
            <h2 style="color: #fff; margin: 100px auto; text-align: center;">ورود / ثبت نام</h2>
        </div>
    </div>
</div>


<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset("style/assets/js/libs/jquery-3.1.1.min.js")}}"></script>
<script src="{{asset("style/bootstrap/js/popper.min.js")}}"></script>
<script src="{{asset("style/bootstrap/js/bootstrap.min.js")}}"></script>

<!-- END GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset("style/assets/js/authentication/form-1.js")}}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    function createTimer(time) {
        var counter = document.getElementById('cycleTimer').getContext('2d');
        var no = time;
        var pointToFill = 0;
        var cw = counter.canvas.width;
        var ch = counter.canvas.height;
        var diff;

        function fillCounter() {
            diff = ((no / time) * Math.PI * 2 * 10);
            counter.clearRect(0, 0, cw, ch);
            counter.lineWidth = 3;
            counter.fillStyle = '#000';
            counter.strokeStyle = '#578600';
            counter.textAlign = 'center';
            counter.font = "10px monospace";
            counter.fillText(no, 30, 33);
            counter.beginPath();
            counter.arc(30, 30, 15, pointToFill, diff / 10 + pointToFill);
            counter.stroke();

            if (no == 0) {
                clearTimeout(fill);
            }
            no--;
        }

        // this.onScreenSizeChanged = function (forceResize) {
        //     if (forceResize || (this.canvas.width != window.innerWidth /*||
        //                     this.canvas.height != window.innerHeight*/)) {
        //
        //         var image = this.context.getImageData(0, 0,
        //             this.canvas.width, this.canvas.height);
        //
        //         this.canvas.width = (window.innerWidth);
        //         this.canvas.height = (window.innerHeight);
        //
        //         this.context.putImageData(image, 0, 0);
        //
        //     }
        // }
        // this.onScreenSizeChanged(true);

        var fill = setInterval(fillCounter, 1000);
    }

    createTimer(120);

    function login() {
        document.getElementById("check-login").disabled = true;
        axios.post('/check-code', {
            user_id: '{{$user_id}}',
            password: document.getElementById('password').value,
        }).then(response => {
            document.getElementById("check-login").disabled = false;
            this.hasCodeError = false;
            if (response.data.status === "failed") {
                document.getElementById('error-text').innerHTML = response.data.message;
            } else if (response.data.status === "success") {
                window.location.href = "/";
            }
        }).catch(error => {
            document.getElementById("check-login").disabled = false;
            alert(error);
        });
    }


    $(function () {
        $('#send-sms-form').each(function () {
            $(this).find('input').keypress(function (e) {
                // Enter pressed?
                if (e.which == 10 || e.which == 13) {
                    login();
                }
            });

            $(this).find('input[type=submit]').hide();
        });
    });
</script>

</body>
</html>
