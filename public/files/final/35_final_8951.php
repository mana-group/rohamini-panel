@extends('panel.inc.app')
@section('css')
    <title>تایید درخواست  - مرادپور</title>
    <link rel="stylesheet" type="text/css" href="{{asset('panel/datatable/datatables.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/dt-global_style.css')}}">
    <link href="https://unpkg.com/persian-datepicker@latest/dist/css/persian-datepicker.min.css" rel="stylesheet">
    {{--    <link rel="stylesheet" href="{{asset('css/chat.css')}}">--}}

    <script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>
@endsection
@section('main')
    <p style="margin-top:2.5em"></p>
    <div class="page-titles"><h3>لیست درخواست ها</h3></div>
    <p style="margin-top:2.5em"></p>
    <div class="card">
        <div class="card-body">
            <div class="card-body">
                <?php $i = 1; ?>
                @foreach($packs as $pack)
                    <h3>پکینگ شماره {{$i}}</h3>
                    <hr>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">اندازه</label>
                                <input type="text" class="form-control" value="{{$pack->size}}" disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">تعداد پالت ها</label>
                                <input type="text" class="form-control" value="{{$pack->pallet_count}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">تعداد کارتن ها در هر پالت</label>
                                <input type="text" class="form-control" value="{{$pack->carton_in_pallet_count}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">تعداد کاشی در هر کارتن</label>
                                <input type="text" class="form-control" value="{{$pack->tile_in_carton_count}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">وزن هر کارتن</label>
                                <input type="text" class="form-control" value="{{$pack->carton_weight}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">متر مربع در هر کارتن</label>
                                <input type="text" class="form-control" value="{{$pack->carton_sqmeter}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">کارتن ها</label>
                                <input type="text" class="form-control" value="{{$pack->carton_count}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">متراژ کل</label>
                                <input type="text" class="form-control" value="{{$pack->sqm_count}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">قیمت واحد</label>
                                <input type="text" class="form-control" value="{{$pack->unit_price}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">قیمت مجموع</label>
                                <input type="text" class="form-control" value="{{$pack->total_price}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">وزن خالص</label>
                                <input type="text" class="form-control" value="{{$pack->net_weight}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">وزن ناخالص</label>
                                <input type="text" class="form-control" value="{{$pack->gross_weight}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="" class="form-control-label">توضیحات</label>
                                <textarea class="form-control" disabled="disabled">{{$pack->description}}</textarea>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                @endforeach
                <?php $i = 1; ?>
                <hr>
                @foreach($drivers as $driver)
                    <h3>راننده شماره {{$i}}</h3>
                    <hr>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">شماره ماشین</label>
                                <input type="text" class="form-control" value="{{$driver->car_number}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">کد ملی راننده</label>
                                <input type="text" class="form-control" value="{{$driver->meli_code}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">نام راننده</label>
                                <input type="text" class="form-control" value="{{$driver->name}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">شماره موبایل</label>
                                <input type="text" class="form-control" value="{{$driver->phone_number}}"
                                       disabled="disabled">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">نوع تریلی</label>
                                <input type="text" class="form-control" value="{{$driver->trailer_type}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">تعداد پالت</label>
                                <input type="text" class="form-control" value="{{$driver->pallet_count}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">شماره ترانزیت</label>
                                <input type="text" class="form-control" value="{{$driver->transit}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">وزن خالی قبض باسکول</label>
                                <input type="text" class="form-control" value="{{$driver->null_weight}}"
                                       disabled="disabled">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="" class="form-control-label">وزن پر قبض باسکول</label>
                                <input type="text" class="form-control" value="{{$driver->full_weight}}"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <a href="{{asset('file/requests/baskol/'.$driver->ghabzebaskol_name)}}"><button class="btn btn-success" type="button">دانلود قبض باسکول</button></a>
                        </div>
                    </div>
                    <?php $i++; ?>
                @endforeach
                @if($myRole->name == "admin" || $myRole->name == "hamyar_admin")
                    {{--<hr>--}}
                    {{--<h4>اطلاعات درخواست</h4>--}}
                    {{--<hr>--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-12 col-md-3">--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="" class="form-control-label">شماره کوتاژ</label>--}}
                    {{--<input type="text" class="form-control" value="{{$eRequest->kotaj_number}}"--}}
                    {{--disabled="disabled">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                @endif
                @if($eRequest->factor_upload)
                    <a href="{{asset('file/requests/factor/'.$eRequest->factor_upload)}}" download><button type="button" class="btn btn-success">دانلود فاکتور</button></a>
                @endif
                <a href="/request/{{$eRequest->id}}/excel"><button class="btn btn-success" type="button">دانلود پکینگ لیست</button></a>
                @if(Auth::user()->roles()->first()->name != "admin" && Auth::user()->roles()->first()->name != "clearance" && Auth::user()->roles()->first()->name != "hamyar_admin")
                    <a href="/request/cancel/{{$eRequest->id}}"><button class="btn btn-danger" type="button">انصراف درخواست</button></a>
                @endif

            </div>
        </div>
    </div>


    <p style="margin-top:2.5em"></p>
    <div class="card">
        <div class="card-body">
            <div class="page-titles"><h3>تکمیل اطلاعات</h3></div>
            <p style="margin-top:2.5em"></p>
            <form action="{{Route('request.verified_reqstore')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="request_id" value="{{$eRequest->id}}" id="request_id">
                <div class="row">
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="" class="form-control-label"><span style="color: red"> * </span>شماره کوتاژ</label>
                            <input type="text" class="form-control" name="kotaj_number"
                                   value="{{$eRequest->kotaj_number}}">
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="" class="form-control-label"><span style="color: red"> * </span>آپلود بیجک</label>
                            <input type="file" class="form-control" name="bijak_upload">
                            @if($eRequest->bijak_upload)<small>ذخیره شده</small>@endif
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="" class="form-control-label"><span style="color: red"> * </span>آپلود اظهارنامه</label>
                            <input type="file" class="form-control" name="declaration_upload">
                            @if($eRequest->declaration_upload)<small>ذخیره شده</small>@endif
                        </div>
                    </div>
                    {{--<div class="col-12 col-md-3">--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="" class="form-control-label">آپلود اظهارنامه</label>--}}
                    {{--<input type="file" class="form-control" name="declaration_upload">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="" class="form-control-label">آپلود گواهی از مبدا</label>
                            <input type="file" class="form-control" name="certification_upload">
                            @if($eRequest->certification_upload)<small>ذخیره شده</small>@endif
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="" class="form-control-label">آپلود نامه هماهنگی</label>
                            <input type="file" class="form-control" name="coordinate_upload">
                            @if($eRequest->coordinate_upload)<small>ذخیره شده</small>@endif
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="" class="form-control-label">آپلود نامه ترانشیپ</label>
                            <input type="file" class="form-control" name="tranship_upload">
                            @if($eRequest->tranship_upload)<small>ذخیره شده</small>@endif
                        </div>
                    </div>
                    <div class="col-12 col-md-3">
                        <div class="form-group">
                            <label for="" class="form-control-label">وضعیت</label>
                            <select class="form-control" name="status">
                                <option disabled selected>وضعیت را مشخص کنید</option>
                                <option @if($eRequest->final_status == 1) selected @endif value="1">ابطال</option>
                                <option @if($eRequest->final_status == 2) selected @endif value="2">وصول</option>
                                <option @if($eRequest->final_status == 3) selected @endif value="3">عدم وصول</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3"></div>
                    <div class="col-12 col-md-12">
                        <p style="margin-top:2.5em"></p>
                        <div class="page-titles"><h3>رد درخواست</h3></div>
                        <p style="margin-top:1.5em"></p>
                    </div>
                    <br><br><br>
                    <div class="col-12 col-md-12">
                        <div class="form-group">
                            <label for="" class="form-control-label">توضیح رد درخواست</label>
                            <textarea type="text" class="form-control" name="dec_description"></textarea>
                        </div>
                    </div>
                    <div class="col-12 col-md-12">

                    </div>
                    <div class="col-12 col-md-5">
                        <button class="btn btn-success" name="submitName" value="store" type="submit">ذخیره</button>
                        <a href="{{Route('request.verify',$eRequest->id)}}" class="btn btn-primary">تایید
                            درخواست</a>
                        <button name="submitName" value="cancel" class="btn btn-danger" type="submit">رد درخواست</button>
                    </div>
                </div>
                <p style="text-align: right"><span style="color: red"> * </span>لطفا ابتدا ذخیره و سپس تایید کنید</p>
            </form>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript" src="{{asset('panel/datatable/datatables.min.js')}}"></script>
    <script>
        @if(Auth::user()->roles()->first()->name == "clearance")
        $(document).ready(function () {
            $('#datatable').DataTable({
                "order": [[2, "desc"]]
            });
        });
        @else
        $(document).ready(function () {
            $('#datatable').DataTable({
                "order": [[7, "desc"]]
            });
        });
        @endif
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".datapicker").pDatepicker({
                format: 'YYYY/MM/DD',
            });
        });
        $('.normal-example').persianDatepicker();
    </script>

    <script src="{{url('plugins/table/datatable/datatables.js')}}"></script>
    <script>
        $('#default-ordering').DataTable({
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
                },
                "sInfo": "صفحه  _PAGE_ از _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "جست و جو ...",
                "sLengthMenu": "تعداد نتایج :  _MENU_",
                "sEmptyTable": "موردی یافت نشد",
                "sInfoEmpty": "هیچ!",
                "sInfoFiltered": " - فیلترشده از _MAX_ سازمان",
                "sZeroRecords": "موردی برای نمایش وجود ندارد!"

            },
            "order": [[3, "desc"]],
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 40],
            "pageLength": 7,
            drawCallback: function () {
                $('.dataTables_paginate > .pagination').addClass(' pagination-style-13 pagination-bordered mb-5');
            }
        });
    </script>

@endsection
