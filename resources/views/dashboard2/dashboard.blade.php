@extends('dashboard2.layout.app')

@section('start')

    <link href="{{("/style/plugins/drag-and-drop/dragula/dragula.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{("/style/plugins/drag-and-drop/dragula/example.css")}}" rel="stylesheet" type="text/css"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="{{("/style/plugins/apex/apexcharts.css")}}" rel="stylesheet" type="text/css">
    <link href="{{("/style/assets/css/dashboard/dash_1.css")}}" rel="stylesheet" type="text/css"/>


    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>


    <link href="{{url('style/plugins/animate/animate.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{url('style/plugins/sweetalerts/promise-polyfill.js')}}"></script>
    <link href="{{url('style/plugins/sweetalerts/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('style/plugins/sweetalerts/sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('style/assets/css/components/custom-sweetalert.css')}}" rel="stylesheet" type="text/css" />


@endsection
@section('main')

    @if ($message = Session::get('phase1'))
        <script type="text/javascript">
            swal("درخواست پروژه جدید شما ارسال شد . ", "منتظر تماس ما باشید.", "success");
        </script>
    @endif
    @if ($message = Session::get('phase2'))
        <script type="text/javascript">
            swal("توضیحات ثبت شد", "منتظر بررسی توسط مشتری باشید", "success");
        </script>
    @endif
    @if ($message = Session::get('acc'))
        <script type="text/javascript">
            swal(" از اعتماد شما صمیمانه متشکریم",
                "سعی داریم تا رضایت کامل شما را بدست آوریم.\n" +
                "تمامی خدمات روح الامین شامل وارانتی عودت وجه می باشد. تا رضایت کامل شما حتی بعد از چاپ همراه شما خواهیم بود و تا 6 ماه، مشمول، بازانجام و در صورت نارضایتی کلی از خدمات ما، حداکثر ظرف مدت 3 روز وجه پرداختی شما، عیناً عودت خواهد گرفت. \n" +
                "با تشکر روح الامین", "success");
        </script>
    @endif
    @if ($message = Session::get('den'))
        <script type="text/javascript">
            swal("ثبت شد", "منتظر تماس باشید...", "success");
        </script>
    @endif
    @if ($message = Session::get('uploadadmin'))
        <script type="text/javascript">
            swal("اپلود شد", "", "success");
        </script>
    @endif
    @if ($message = Session::get('pay'))
        <script type="text/javascript">
            swal("پرداخت شد", "از اینکه چند دقیقه وقت با ارزش خود را به بهود خدمات ما اختصاص می دهید صمیمانه سپاسگزاریم. اگر شکایت، انتقاد و یا پیشنهادی در مورد ما دارید خوش حال می شویم آن را با ما در انتهای فر م نظر سنجی به اشتراک بگذارید و در امتیاز بندی تخفیفات ما سهیم باشید.", "success");
        </script>
    @endif
    @if ($message = Session::get('nazar'))
        <script type="text/javascript">
            swal("نظر شما ثبت شد", "اکنون فایل نهایی را دانلود کنید", "success");
        </script>
    @endif

    @if(Session::get('new'))
        @if(Session::get('new')==1)
            {{--<script type="text/javascript">--}}
            {{--swal("خوش آمدید", "به رغم اعتماد شما 30 هزار تومان به کیف پول شما اضافه شد. ", "success");--}}
            {{--</script>--}}
            <script type="text/javascript">
                setTimeout(function () {
                    swal("خوش آمدید", "به رغم اعتماد شما 30 هزار تومان به کیف پول شما اضافه شد. \n با تکمیل اطلاعات خود مبلغ 30 هزار تومان کیف پول خود را شارژ کنید. شما میتوانید در تسویه نهایی پروژه کیف پول خود را اعمال نمایید .  ", "success");
                }, 5000);
            </script>
        @endif
    @endif

    <div class="row" id="cancel-row">

        <div class="col-lg-12 layout-spacing">

                <div class="widget-content widget-content-area">

                    <div class='row'>
                        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing" style="margin-bottom: -120px">

                            {{--<div class="widget widget-chart-two">--}}
                            <div class="widget-heading">
                                <h5 class="">آمار پروژه ها</h5>
                            </div>
                            <div class="widget-content">
                                <div id="chart" class=""></div>
                            </div>
                            {{--</div>--}}
                        </div>

                        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">


                            <div class="widget-heading">
                                <h5 class="">پروژه هایی که باید بررسی کنید:</h5>
                            </div>

                            <table id="zero-config" class="table table-hover" style="width:100%">
                                <?php $i = 1 ?>
                                <thead>
                                <tr>
                                    <th>ردیف</th>
                                    <th>کد</th>
                                    @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                        <th>مشتری</th>
                                        <th>پرداختیهای مشتری</th>
                                    @endif
                                    <th>موضوع</th>
                                    <th>سطح</th>
                                    <th>مشاهده</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>
                                            {{$request->id}}
                                        </td>

                                        @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))

                                            <td>
                                                {{--                                        <a href="/user/{{$request['user']->id}}" style="color: blue">--}}
                                                {{--                                            {{$request['user']->name." ". $request['user']->family}}--}}
                                                {{--                                        </a>--}}
                                            </td>
                                            <td>
                                                {{$request['paid']}}
                                            </td>
                                        @endif
                                        <td>
                                            {{\App\Models\RequestType::where('id' , $request->type)->first()->name}}
                                        </td>

                                        <td>
                                            {{\App\Models\TypeLevel::where('id' , $request->level)->first()->name}}
                                        </td>
                                        <td>
                                            <a href="/request/show/{{$request->id}}">
                                                <i data-feather="eye"
                                                   @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                                                   @if($request->seen==1 || $request->seen==3)
                                                   style="color: gray"
                                                   @else
                                                   style="color: blue"
                                                   @endif
                                                   @else
                                                   @if($request->seen==2 || $request->seen==3)
                                                   style="color: gray"
                                                   @else
                                                   style="color: blue"
                                                    @endif
                                                    @endif
                                                ></i>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>
        </div>


    </div>



@endsection

@section('end')

    <script src="{{("/style/plugins/drag-and-drop/dragula/dragula.min.js")}}"></script>
    <script src="{{("/style/plugins/drag-and-drop/dragula/custom-dragula.js")}}"></script>
    <script src="{{("/style/plugins/apex/apexcharts.min.js")}}"></script>
    <script src="{{("/style/assets/js/dashboard/dash_1.js")}}"></script>

    <script>
        var options = {
            chart: {
                type: 'donut'
            },

            series: [{{$end}},{{$not}}],
            labels: ['تمام شده', 'ناتمام']


        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);

        chart.render();
    </script>


    <script src="{{url('style/plugins/sweetalerts/sweetalert2.min.js')}}"></script>
    <script src="{{url('style/plugins/sweetalerts/custom-sweetalert.js')}}"></script>

    @if($firstLogin)
    <script>

        swal({
            title: 'تبریک!',
            text: "از اعتماد شما به ما بسیار متشکریم. برای قدردانی از اعتماد شما حساب کاربری شما 30 هزار تومان شارژ شد!",
            type: 'success',
            padding: '2em'
        })

    </script>
    @endif
    @if(\Illuminate\Support\Facades\Auth::user()->name == '' || \Illuminate\Support\Facades\Auth::user()->family == '')
        <script>
            setTimeout(function(){

                swal({
                    title: 'پروفایلت رو کامل کن!',
                    type: 'info',
                    html:
                        'لطفا برای ارائه هر چه بهتر خدمات اطلاعات لازم در پروفایل کاربری خود را کامل کنید! در نظر داشته باشید که درصورتی که اطلاعات کاربری خود را تکمیل کنید 30 هزار تومان دیگر به عنوان هدیه به حساب کاربری شما اضافه خواهد شد.',
                    showCloseButton: true,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText:
                        '<i class="flaticon-checked-1"></i> تکمیل اطلاعات پروفایل',
                    confirmButtonAriaLabel: 'تکمیل اطلاعات پروفایل',
                    cancelButtonText:
                        '<i class="flaticon-cancel-circle"></i> فعلا نه',
                    cancelButtonAriaLabel: 'فعلا نه',
                    padding: '2em'
                }).then(function(result) {
                    if (result.value) {

                        window.location.href = "{{url('user/'.\Illuminate\Support\Facades\Auth::user()->id)}}";

                    }
                })

            }, 15000); //run this after 3 seconds
        </script>
    @endif

@endsection
