<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyUser extends Model
{
    use HasFactory;
    public function getScoreNameAttribute()
    {
        if ($this->score == 1)
            return 'عالی';
        elseif ($this->score == 2) {
            return 'خوب';
        } elseif ($this->score == 3) {
            return 'متوسط';
        } elseif ($this->score == 4) {
            return 'بد';
        } elseif ($this->score == 5) {
            return 'خیلی بد';
        }

    }
}
