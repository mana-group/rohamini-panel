<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('/sendPass', [\App\Http\Controllers\web\AuthController::class, 'sendPass'])->name('sendPass');
Route::any('/check-code', [\App\Http\Controllers\web\AuthController::class, 'checkCode'])->name('checkCode');
Route::any('/login', [\App\Http\Controllers\web\AuthController::class, 'login'])->name('login');
Route::get('/reg', [\App\Http\Controllers\web\AuthController::class, 'reg']);
Route::post('/register', [\App\Http\Controllers\web\AuthController::class, 'register']);
Route::get('/logout', [\App\Http\Controllers\web\AuthController::class, 'logout']);

Route::middleware('auth')->group(function () {

    Route::get('/', [\App\Http\Controllers\web\DashboardController::class, 'dashboard']);

//    user
    Route::get('/users', [\App\Http\Controllers\web\UserController::class, 'list']);
    Route::get('/user/remove/{id}', [\App\Http\Controllers\web\UserController::class, 'remove']);
    Route::get('/user/{id}', [\App\Http\Controllers\web\UserController::class, 'profile']);
    Route::post('/user/edit/{id}', [\App\Http\Controllers\web\UserController::class, 'edit']);

//    request
    Route::get('/request', [\App\Http\Controllers\web\RequestController::class, 'request']);
    Route::get('/request/list', [\App\Http\Controllers\web\RequestController::class, 'requestList']);
    Route::get('/request/show/{id}', [\App\Http\Controllers\web\RequestController::class, 'requestShow']);
    Route::post('/request/description/{id}', [\App\Http\Controllers\web\RequestController::class, 'requestDescription']);
    Route::post('/request/accessdenied/{id}', [\App\Http\Controllers\web\RequestController::class, 'requestAccDen']);
    Route::post('/request/finaladmin/{id}', [\App\Http\Controllers\web\RequestController::class, 'requestFinalAdmin']);
    Route::post('/request/paid/{id}', [\App\Http\Controllers\web\RequestController::class, 'requestPay']);
    Route::post('/request/nazar/{id}', [\App\Http\Controllers\web\RequestController::class, 'requestNazar']);
    Route::post('/request/create', [\App\Http\Controllers\web\RequestController::class, 'requestCreate']);
    Route::post('/request/accept-pay/{id}', [\App\Http\Controllers\web\RequestController::class, 'requestAcceptPay']);

    //    survey
    Route::get('/surveys', [\App\Http\Controllers\web\SurveyController::class, 'survays']);
    Route::get('/survey/{id}', [\App\Http\Controllers\web\SurveyController::class, 'editShow']);
    Route::post('/survey/{id}', [\App\Http\Controllers\web\SurveyController::class, 'edit']);
    Route::get('/survey/delete/{id}', [\App\Http\Controllers\web\SurveyController::class, 'delete']);
    Route::post('/surveys/create', [\App\Http\Controllers\web\SurveyController::class, 'create']);



});

