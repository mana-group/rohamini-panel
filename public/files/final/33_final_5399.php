<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/test', 'panelController@test')->name('test');

Route::get('/login', 'AuthController@newLoginPage')->name('login');
Route::post('/login', 'AuthController@newLogin');
Route::post('/sendTokenToServer','AuthController@sendTokenToServer');
//Route::get('/test','PanelController@push');
Route::get('/location',"PanelController@location");
Route::group(['middleware' => 'checklogin'], function () {
    Route::get('/register', 'AuthController@registerForm')->name('registerForm');
    Route::post('/register', 'AuthController@register')->name('register');
    Route::post('/register/show/inputs', 'AuthController@registerShowInputs')->name('register.showinputs');
    Route::get('/last-login', 'AuthController@loginPage');
    Route::post('/last-login', 'AuthController@login');
    Route::post('/check-code','AuthController@checkCode');

});
Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'AuthController@logout')->name('logout');
    Route::get('/', 'PanelController@index')->name('panel.index')->middleware('badge');

    //CHAT
    Route::get('/chat','ChatController@index');
    Route::get('/get-chats','ChatController@getChats');
    Route::post('/send-message','ChatController@sendMessage');
    Route::get('/get-messages/{chat_id?}','ChatController@getMessages');
    Route::get('/contacts','ChatController@getContacts');
    Route::post("/start-chat",'ChatController@startChat');
    Route::post('/remove-message','ChatController@removeMessage');
    //

    Route::get('/statistics','PanelController@getStatistics')->middleware('badge');
    Route::post('/statistics','PanelController@statistics')->middleware('badge');

//    Route::post('/checkPlayerId','PanelController@checkPlayerId');

    /*Factory Pages*/
    Route::group(['middleware' => ['role:admin|hamyar_admin']], function () {
        Route::get('/factory/list', 'PanelController@factoryList')->name('factory.list')->middleware('badge');
        Route::post('/factory/status', 'PanelController@factoryCStatus')->name('factory.cstatus');
        Route::get('/profile/{id}','PanelController@factory_profile')->middleware('badge');
        Route::get('/wallet/{id}','WalletController@factoryWallet')->middleware('badge');
    });

    /*Request Pages*/
    Route::group(['middleware' => ['role:manager|commerce|export']], function () {
        Route::get('/request/create', 'PanelController@requestCreate')->name('request.create')->middleware('badge');
        Route::post('/request/store', 'PanelController@requestStore')->name('request.store');
        Route::post('/request/driver/search', 'PanelController@searchForDriver');
        Route::get('/request/cancel/{id}','PanelController@cancel_request');
        Route::get('/request/accept/cancel/{id}','PanelController@accept_admin_decline');
        Route::post('/request/resend/{id}','PanelController@resendRequest');
        Route::get('/invoice','PanelController@invoicePage')->name('invoice.page')->middleware('badge');
        Route::get('/invoice/{id}','PanelController@searchForInvoice')->name('invoice')->middleware('badge');
    });
    Route::group(['middleware' => ['role:admin|manager|commerce|financial|export|tolidi|hamyar_admin']], function () {
        Route::get('/request/list/NotVerified', 'PanelController@requestNotVerifiedList')->name('request.list_not_verified')->middleware('badge');
        Route::get('/request/{id}/verify', 'PanelController@requestVerified')->name('request.verified_req')->middleware('badge');
        Route::post('/request/verify/store', 'PanelController@requestVerifiedStore')->name('request.verified_reqstore');
        Route::get('/request/{id}/verified', 'PanelController@requestVerify')->name('request.verify')->middleware('badge');
        Route::get('/request/{id}/resend', 'PanelController@requestResend')->name('request.verify')->middleware('badge');
        Route::post('/request/add/tarkhis','PanelController@addTarkhiskar');
        Route::post('/request/add/tarkhisupdate','PanelController@updateTarkhiskar');
        Route::post('/request/add/checkmobile','PanelController@checkmobile');
        Route::post('/request/tarkhiskars/{id}','PanelController@tarkhiskars');
    });

    Route::group(['middleware' => ['role:hamyar_admin','badge']],function (){
        Route::get('/hamyar/statistics','PanelController@hamyarStatisticsPage')->name('hamyar.statistics')->middleware('badge');
        Route::get('/hamyar/statistics/{id}','PanelController@hamyarStatisticsResult')->middleware('badge');
    });

    Route::get('/request/list/{new?}', 'PanelController@requestVerifiedList')->name('request.list_verified')->middleware('badge');
    Route::get('/request/{id}/show', 'PanelController@requestShow')->name('request.show')->middleware('badge');

    /*User Pages*/
    Route::group(['middleware' => ['role:admin|manager|hamyar_admin']], function () {
        Route::get('/user/list', 'PanelController@userList')->name('user.list')->middleware('badge');
//        Route::get('/user/edit/{id}','PanelController@editUserForm')->name('user.edit.form')->middleware('badge');
        Route::any('/user/edit/{id}','PanelController@editUser')->name('user.edit')->middleware('badge');
        Route::post('/user/store', 'PanelController@userStore')->name('user.store')->middleware('badge');
        Route::post('/user/status', 'PanelController@userCStatus')->name('user.cstatus')->middleware('badge');
    });
    Route::group(['middleware' => ['role:manager']], function () {
        Route::get('/user/create', 'PanelController@userCreate')->name('user.create')->middleware('badge');
        Route::post('/user/show/inputs', 'PanelController@userSelectInputs')->name('user.showinputs');


        Route::get('/profile','PanelController@getProfile')->middleware('badge');
        Route::post('/profile','PanelController@changeProfile');
        Route::post('/token','PanelController@getToken');
    });

    Route::group(['prefix' => 'wallet','middleware' => ['role:admin|hamyar_admin|financial|manager']],function () {
        Route::get('list','WalletController@getTransactions')->name('wallet.list')->middleware('badge');
        Route::get('charge','WalletController@getChargeWallet')->name('wallet.charge')->middleware('badge');
        Route::post('charge','WalletController@postChargeWallet')->middleware('badge');
    });

    Route::group(['prefix' => 'financial','middleware' => ['role:admin|hamyar_admin|financial|manager']],function () {
        Route::get('turnover/{id?}','PanelController@turnover')->name('financial.turnover')->middleware('badge');
        Route::any('payment','PanelController@payment')->name('financial.payment')->middleware('badge','role:financial|manager');
        Route::get('report','PanelController@financialReport')->name('financial.report')->middleware('badge');
        Route::post('fee','PanelController@updateFee');
    });

    Route::get('request/{id}/excel','PanelController@downloadExcel');
    Route::get('baskol/{id}/{i}','PanelController@Baskol');

    Route::get('invoice/{id}/excel','PanelController@downloadInvoice');


//import
    Route::group(['prefix' => '/import' , 'middleware' => ['badge']], function () {
        Route::get('/create', 'ImportController@importCreate')->name('import.create');
        Route::post('/store', 'ImportController@importStore')->name('import.store');
        Route::get('/list', 'ImportController@importsList')->name('import.list');
        Route::get('/list-verified', 'ImportController@importsVrtifiedList')->name('import.list_verified');
        Route::get('/show/{id}', 'ImportController@importShow')->name('import.show');
        Route::any('/edit/{id}', 'ImportController@importEdit')->name('import.edit');
        Route::post('/driver/{id}','ImportController@importDriver')->name('import.driver');
        Route::post('/message/{id}','ImportController@sendMessage')->name('import.message.send');
        Route::post('/optional/{id}','ImportController@sendOptional')->name('import.optional.send');
        Route::post('/confirm/{id}','ImportController@confirm')->name('import.confirm');
    });

    //letter
    Route::group(['prefix' => '/letter' , 'middleware' => ['badge']], function ()
    {
        Route::any('/type/create', 'LetterController@typecreate');
        Route::any('/type/list/{id?}', 'LetterController@typelist');
        Route::any('/type/delete/{id}', 'LetterController@delete');
        Route::any('/type/edit/{id}', 'LetterController@edit');

        Route::get('/notverified_list', 'LetterController@notverifiedlist');
        Route::get('/verified_list', 'LetterController@verifiedlist');
        Route::any('/create', 'LetterController@create');
        Route::get('/show/{id}', 'LetterController@show');
        Route::post('/accept/{id}', 'LetterController@accept');
        Route::get('/merge/{id}', 'LetterController@merge');

    });

    //fee
    Route::group(['prefix' => '/fee' , 'middleware' => ['badge']], function ()
    {
        Route::get('/{id}', 'FeeController@list');
        Route::post('/create/{id}', 'FeeController@create');
        Route::get('/delete/{id}', 'FeeController@delete');
    });

    //Inspection
    Route::group(['prefix' => '/inspection' , 'middleware' => ['badge']], function ()
    {
        Route::get('/notverified_list', 'InspectionController@notverifiedlist');
        Route::get('/verified_list', 'InspectionController@verifiedlist');
        Route::any('/create', 'InspectionController@create');
        Route::get('/show/{id}', 'InspectionController@show');
        Route::post('/accept/{id}', 'InspectionController@accept');

    });

    //message dashboard
    Route::group(['prefix' => '/message','middleware' => ['role:hamyar_admin']],function () {
        Route::any('/create','DashboardMessageController@create')->middleware('badge');
        Route::get('/list','DashboardMessageController@all')->middleware('badge');
        Route::get('/delete/{id}','DashboardMessageController@delete')->middleware('badge');

    });

    Route::group(['middleware' => ['role:admin|manager|commerce|financial|export|tolidi|hamyar_admin']], function () {
    });

//    Route::group(['prefix' => '/border'], function () {
    Route::group(['prefix' => '/border'], function () {

        Route::get('/', 'BorderController@list')->name('border.list')->middleware('badge');
        Route::get('/edit/{id}', 'BorderController@edit')->middleware('badge');
        Route::post('/edit/{id}', 'BorderController@postEdit')->middleware('badge');
//        Route::get('/', 'BorderController@list');
        Route::post('/add/{id}', 'BorderController@add')->middleware('badge');
        Route::post('/country', 'BorderController@country')->middleware('badge');

    });

});



