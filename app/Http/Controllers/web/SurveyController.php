<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Survey;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    //
    public function survays()
    {
        $requests=Survey::all();
        return view('dashboard2.survey.list',compact('requests'));
    }

    public function editShow($id)
    {
        $requests=Survey::all();
        $serv=Survey::findOrFail($id);
        return view('dashboard2.survey.list',compact('requests','serv'));
    }

    public function edit(Request $request,$id)
    {

        $serv=Survey::findOrFail($id);
        $serv->question=$request->question;
        $serv->save();

        return redirect('/surveys')->with('success', 'ویرایش شد');
    }
    public function delete(Request $request,$id)
    {

        $serv=Survey::findOrFail($id);
        $serv->delete();

        return redirect('/surveys')->with('success', 'حذف شد');
    }

    public function create(Request $request)
    {
        $serv=new Survey();
        $serv->question=$request->question;
        $serv->save();

        return redirect('/surveys')->with('success', 'ثبت شد');
    }
}
