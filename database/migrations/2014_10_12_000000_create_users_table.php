<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('family')->nullable();
            $table->tinyInteger('gender')->nullable()->comment('0 female  1 male');
            $table->string('email')->unique()->nullable();
            $table->string('national')->unique()->nullable();
            $table->string('shenasname')->nullable();
            $table->string('address')->nullable();
            $table->string('postal')->nullable();
            $table->string('tel')->nullable();
            $table->string('mobile')->unique();
            $table->string('image')->nullable();
            $table->string('account')->default(0);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
