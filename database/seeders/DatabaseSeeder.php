<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->display_name = 'Admin';
        $role->save();

        $role = new Role();
        $role->name = 'user';
        $role->display_name = 'user';
        $role->save();


        $user = new User();
        $user->name = "admin";
        $user->mobile = "0";
        $user->password = bcrypt("12345");
        $user->save();
        $role = \App\Models\Role::where("name", "admin")->first();
        $user->attachRole($role);

        $user = new User();
        $user->name = "user";
        $user->mobile = "1";
        $user->password = bcrypt("12345");
        $user->save();
        $role = Role::where("name", "user")->first();
        $user->attachRole($role);
    }
}
