<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\RequestType;
use App\Models\Survey;
use App\Models\SurveyUser;
use App\Models\Turnover;
use App\Models\TypeLevel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\Jalalian;
use SoapClient;
use Validator;
use DB;

class RequestController extends Controller
{
    //

    public function sendSms($text1, $text2, $rec, $body)
    {
        $text = $text1 . ";" . $text2;
        ini_set("soap.wsdl_cache_enabled", "0");
        $sms = new SoapClient("http://api.payamak-panel.com/post/Send.asmx?wsdl", array("encoding" => "UTF-8"));
        $data = array(
            "username" => "09130999094",
            "password" => "5243",
            "text" => "$text",
            "to" => $rec,
            "from" => "50004001999094",
            "bodyId" => $body
        );
        $send_Result = $sms->SendByBaseNumber2($data)->SendByBaseNumber2Result;
        return $send_Result;


    }

    public function sms()
    {
        return $this->sendSms('a', 's', 'd', 'f', "09133934677", "46632");
    }

    public function request()
    {
        return view('dashboard2.request.request');
    }

    public function requestShow($id)
    {
        $surveys = Survey::all();
        $request = \App\Models\Request::findOrFail($id);
//        $time = explode("-", $request->date);
//        $day = explode(" ", $time[2]);
//
//        $time = \Morilog\Jalali\CalendarUtils::toJalali($time[0], $time[1], $day[0]); // [1395, 2, 18]

//        $t = $time[0] . '/' . $time[1] . '/' . $time[2];
        $t = $request->date;
//        return $t;
        $survs = null;
        if ($request->status == 7) {
            $survs = SurveyUser::where('request_id', $request->id)->get();
            foreach ($survs as $surv) {
                $qu = Survey::where('id', $surv->question_id)->first();;
                $surv['question'] = $qu->question;
                $surv->append('ScoreName')->toArray();

            }
        }
        $user = Auth::user();
        if ($request->status == 1) {
            if ($user->hasRole('admin')) {
                $request->seen = 3;
            }
        }
        if ($request->status == 2) {
            if (!$user->hasRole('admin')) {
                $request->seen = 3;
            }
        }
        if ($request->status == 3) {
            if ($user->hasRole('admin'))
                $request->seen = 3;
        }
        if ($request->status == 4) {
            if ($user->hasRole('admin'))
                $request->seen = 3;
        }
        if ($request->status == 5) {
            if (!$user->hasRole('admin'))
                $request->seen = 3;
        }
        $request->save();
//        return $survs;
        return view('dashboard2.request.request', compact('request', 't', 'surveys', 'survs'));
    }

    public function requestCreate(Request $request)
    {

        $data = $request->all();

        $rule = [
            'priority' => 'required',
            'type' => 'required',
            'number' => 'required',
            'date' => 'required',
        ];
        $message = [
//            "national.unique" => "کد ملی تکراری است",
        ];
        $valid = Validator::make($data, $rule, $message);
        if ($valid->fails())
            return redirect()->back()->withErrors($valid)->withInput();


        $time = explode("/", $request->date);
        $time = (new Jalalian($this->convert($time[0]), $this->convert($time[1]), $this->convert($time[2]), 0, 0, 0))->toCarbon()->toDateTimeString();


        $req = new \App\Models\Request();
        $req->user_id = Auth::user()->id;
        $req->type = $request->type;
        $req->level = $request->level;
        $req->num = $request->number;
        $req->priority = $request->priority;
        $req->date = $time;
        $req->status = '1';
        $req->seen = '2';
        $req->price_taghribi = TypeLevel::where('id' , $request->level)->firstOrFail()->price * $request->number;

        $req->save();
        $admin = User::whereRoleIs('admin')->first();
        $this->sendSms(Auth::user()->name . " " . Auth::user()->family, $req->id, $admin->mobile, "46632");

        return redirect('/')->with('phase1', 'ثبت شد');

    }

    public function requestDescription(Request $request, $id)
    {
        $data = $request->all();

        $rule = [
            'description' => 'required',
            'final_price' => 'required',

        ];
        $message = [
//            "national.unique" => "کد ملی تکراری است",
        ];
        $valid = Validator::make($data, $rule, $message);
        if ($valid->fails())
            return redirect()->back()->withErrors($valid)->withInput();


        $req = \App\Models\Request::findOrFail($id);
        $req->status = '2';
        $req->seen = '1';
        $req->price_final = $request->final_price;
        $req->description = $request->description;

        $req->save();

        $admin = User::find($req->user_id);
        $this->sendSms(Auth::user()->name . " " . Auth::user()->family, $req->id, $admin->mobile, "46632");

        return back()->with('phase2', 'ثبت شد');

    }

    public function requestAccDen(Request $request, $id)
    {
        $req = \App\Models\Request::findOrFail($id);
        if ($request->action == 'با من تماس بگیرید') {
            $req->seen = '2';

            $req->status = '3';
            $req->save();
            $admin = User::whereRoleIs('admin')->first();
            $this->sendSms(Auth::user()->name . " " . Auth::user()->family, $req->id, $admin->mobile, "46632");

            return back()->with('den', 'ثبت شد');
        } else {
            $req->status = '4';
            $req->seen = '2';

            $file_upload = $request->file('file');
            if ($file_upload != null) {
                $pathName = $id . '_fish_' . rand(1000, 9999) . '.' . $file_upload->getClientOriginalExtension();
                $file_upload->move('files/prefishs', $pathName);

                $req->pish_fish = $pathName;
            } else {
                return back();
            }

        }


        $req->save();
        $price = round(str_replace(',', '', $req->price_final) * 0.3, 0);

        if ($request->has("preusewallet")){
            $user = User::where('id' , Auth::user()->id)->firstOrFail();
            $user->account = $user->account - $price;
            if ($user->account < 0){
                $user->account = 0;
            }
            $user->save();
        }

        $turn = new Turnover();
        $turn->user_id = Auth::user()->id;
        $turn->request_id = $req->id;
        $turn->price = $price;
        $turn->description = '1';
        $turn->save();

        $admin = User::whereRoleIs('admin')->first();
        $this->sendSms(Auth::user()->name . " " . Auth::user()->family, $req->id, $admin->mobile, "46632");
        return back()->with('acc', 'ثبت شد');

    }

    public function requestFinalAdmin(Request $request, $id)
    {
        $req = \App\Models\Request::findOrFail($id);
        $req->status = '5';
        $req->seen = '1';

        $file_upload = $request->file('file');
        if ($file_upload != null) {
            $pathName = $id . '_final_' . rand(1000, 9999) . '.' . $file_upload->getClientOriginalExtension();
            $file_upload->move('files/final', $pathName);

            $req->file_final = $pathName;
        }
//return $req;

        $req->save();
        $admin = User::find($req->user_id)->first();
        $this->sendSms(Auth::user()->name . " " . Auth::user()->family, $req->id, $admin->mobile, "46632");

        return back()->with('uploadadmin', 'ثبت شد');
    }

    public function requestPay(Request $request, $id)
    {
        $req = \App\Models\Request::findOrFail($id);
        $req->status = '6';
        $req->seen = '2';

        $file_upload = $request->file('file');
        if ($file_upload != null) {
            $pathName = $id . '_fish_' . rand(1000, 9999) . '.' . $file_upload->getClientOriginalExtension();
            $file_upload->move('files/fishs', $pathName);

            $req->final_fish = $pathName;
        } else {
            return back();
        }

        $user = User::where('id' , Auth::user()->id)->firstOrFail();
        $wallet = $user->account;

        if ($request->has('finalusewallet')){
            $wallet = $user->account - round(str_replace(',', '', $req->price_final) * 2 / 3, 0);
            if ($wallet < 0){
                $wallet = 0;
            }
        }

        $req->save();
        $user = Auth::user();
        $price = round(str_replace(',', '', $req->price_final) / 10, 0);
        $wallet += $price;
        $user->account = $wallet;
        $turn = new Turnover();
        $turn->user_id = $user->id;
        $turn->request_id = $req->id;
        $turn->price = round(str_replace(',', '', $req->price_final) * 0.7, 0);;
        $turn->description = '2';
        $turn->save();

        $user->save();

        $admin = User::whereRoleIs('admin')->first();
        $this->sendSms(Auth::user()->name . " " . Auth::user()->family, $req->id, $admin->mobile, "46632");
        return back()->with('pay', 'از اینکه چند دقیقه وقت با ارزش خود را به بهود خدمات ما اختصاص می دهید صمیمانه سپاسگزاریم. اگر شکایت، انتقاد و یا پیشنهادی در مورد ما دارید خوش حال می شویم آن را با ما در انتهای فر م نظر سنجی به اشتراک بگذارید و در امتیاز بندی تخفیفات ما سهیم باشید.');


    }

    public function requestAcceptPay(Request $request, $id)
    {
        $req = \App\Models\Request::findOrFail($id);
        $req->is_finished = '1';
        $req->seen = '3';
        $req->save();
        return back();

    }

    public function requestNazar(Request $request, $id)
    {
//        return $request;

        $surveys = Survey::all();
        foreach ($surveys as $key => $survey) {
            $sur = new SurveyUser();
            $sur->request_id = $id;
            $sur->question_id = $survey->id;
            $sur->score = $request->answer[$survey->id];
            $sur->save();
        }

        $req = \App\Models\Request::findOrFail($id);
        $req->status = '7';
        $req->save();
//        $req->user_survey = $request->user_survey;
        $req->seen = '2';
        $req->save();
        return back()->with('nazar', 'ثبت شد');

    }


    public function requestList(Request $request)
    {
        $title = '';
//        return $request->fin;
        $user = Auth::user();
        if ($user->hasRole('admin'))
            $requests = \App\Models\Request::orderBy('id', 'desc')->get();
        else
            $requests = \App\Models\Request::where('user_id', $user->id)->orderBy('id', 'desc')->get();
        if (isset($request->fin)) {
            if ($request->fin == 1) {
                $title = 'درخواست های تمام شده';
                $requests = $requests->where('status', '7');
            } else {
                $title = 'درخواست های در دست اقدام';
                $requests = $requests->where('status', '!=', '7');
            }

        } else {
            $title = 'همه درخواست ها';
        }

        foreach ($requests as $request) {
            $request['user'] = User::find($request->user_id);
            $request['paid'] = Turnover::where('user_id', $request->user_id)->sum('price');
            $request->append('TypeName')->toArray();
        }

        return view('dashboard2.request.list', compact('requests' , 'title'));
    }

    function convert($string)
    {
        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١', '٠'];

        $num = range(0, 9);
        $convertedPersianNums = str_replace($persian, $num, $string);
        $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

        return $englishNumbersOnly;
    }
}
