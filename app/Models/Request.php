<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    public function getTypeNameAttribute() {
        if ($this->type == 1)
            return  'لوگو';
        else if ($this->type == 2) {
            return 'فلان';

        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
